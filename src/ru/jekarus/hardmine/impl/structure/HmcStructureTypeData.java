package ru.jekarus.hardmine.impl.structure;

import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;
import ru.jekarus.hardmine.api.structure.type.StructureTypeTitle;

public class HmcStructureTypeData implements StructureTypeData {

    private final JavaPlugin owner;
    private final StructureTypeTitle title;

    public HmcStructureTypeData(JavaPlugin owner, String title)
    {
        this.owner = owner;
        this.title = StructureTypeTitle.newTitle(title);
    }

    @Override
    public JavaPlugin getOwner()
    {
        return this.owner;
    }

    @Override
    public StructureTypeTitle getTitle()
    {
        return this.title;
    }

}
