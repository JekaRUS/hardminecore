package ru.jekarus.hardmine.api.structure;

import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;

public interface Structure extends StructureSerializer, StructureDeserializer {

    StructureUser getOwner();

    StructureAccess getAccess();

    StructureData getData();

    StructureType getType();

    StructureUniqueId getUniqueId();

}
