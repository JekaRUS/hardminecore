package ru.jekarus.hardmine.impl;

import org.bukkit.Material;
import ru.jekarus.hardmine.api.StructureTypeContainer;
import ru.jekarus.hardmine.api.exception.StructureAlreadyRegisteredException;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;
import ru.jekarus.hardmine.api.structure.type.StructureTypeTitle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class HmcStructureTypeContainer implements StructureTypeContainer {

    private Map<StructureTypeTitle, StructureType> structures;
    private Map<Material, Collection<StructureType>> interactedBlocks;

    public HmcStructureTypeContainer()
    {
        this.structures = new HashMap<>();
        this.interactedBlocks = new HashMap<>();
    }

    @Override
    public void register(StructureType type)
    {
        if (type == null)
        {
            throw new NullPointerException();
        }
        StructureTypeData data = type.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureTypeTitle title = data.getTitle();
        if (title == null)
        {
            throw new NullPointerException();
        }
        if (this.structures.containsKey(title))
        {
            throw new StructureAlreadyRegisteredException(type);
        }
        this.structures.put(title, type);
        for (SchematicBlock schematicBlock : type.getInteractedBlocks())
        {
            Collection<StructureType> interactedBlocks = this.interactedBlocks.computeIfAbsent(schematicBlock.asBukkitMaterial(), k -> new ArrayList<>());
            interactedBlocks.add(type);
        }
    }

    @Override
    public void unregister(StructureType type)
    {
        if (type == null)
        {
            throw new NullPointerException();
        }
        StructureTypeData data = type.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureTypeTitle title = data.getTitle();
        if (title == null)
        {
            throw new NullPointerException();
        }
        if (this.structures.containsKey(title))
        {
            throw new StructureAlreadyRegisteredException(type);
        }
        this.structures.remove(type.getData().getTitle());
        for (SchematicBlock schematicBlock : type.getInteractedBlocks())
        {
            Collection<StructureType> interactedBlocks = this.interactedBlocks.get(schematicBlock.asBukkitMaterial());
            if (interactedBlocks != null)
            {
                interactedBlocks.remove(type);
            }
        }
    }

    public Map<Material, Collection<StructureType>> asInteractedBlocks()
    {
        return this.interactedBlocks;
    }

}