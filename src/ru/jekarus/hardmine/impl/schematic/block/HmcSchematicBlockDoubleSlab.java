package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Step;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockDoubleSlab;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.schematic.block.utils.SlabMaterials;

public class HmcSchematicBlockDoubleSlab implements SchematicBlockDoubleSlab {

    protected SlabMaterials material;

    public HmcSchematicBlockDoubleSlab()
    {
        this(SlabMaterials.ANY);
    }

    public HmcSchematicBlockDoubleSlab(SlabMaterials material)
    {
        this.material = material;
    }

    @Override
    public SlabMaterials getMaterial()
    {
        return this.material;
    }

    @Override
    public void setMaterial(SlabMaterials material)
    {
        this.material = material;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return this.material.asBukkitDouble();
    }

    @Override
    public void place(Block block)
    {
        block.setType(this.material.asBukkitDouble());
        Step slab = this.asBukkitSlab(block);
        slab.setMaterial(this.material.getMaterial());
    }

    @Override
    public boolean compare(Block block)
    {
        if (!this.material.equalsTypeDouble(block.getType()))
        {
            Bukkit.broadcastMessage("invalid type - " + block.getType() + " - need - " + this.material.asBukkit());
            return false;
        }
        Step slab = this.asBukkitSlab(block);
        if (!this.material.equalsMaterial(slab.getMaterial()))
        {
            return false;
        }
        return true;
    }

    private Step asBukkitSlab(Block block)
    {
        return (Step) block.getState().getData();
    }

    @Override
    public SchematicBlockDoubleSlab cloneBlock()
    {
        return new HmcSchematicBlockDoubleSlab(this.material);
    }

    @Override
    public SchematicBlockDoubleSlab cloneBlock(BlockRotate rotate)
    {
        return this.cloneBlock();
    }
}
