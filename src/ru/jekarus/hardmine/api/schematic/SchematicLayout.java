package ru.jekarus.hardmine.api.schematic;

import org.bukkit.block.Block;

public interface SchematicLayout {

    boolean checkStructure(Block block);

    boolean checkChangedBlock(Block minBlock, Block changedBlock);
}
