package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.ChestMaterials;

public interface SchematicBlockChest extends SchematicBlockFaced<SchematicBlockChest> {

    ChestMaterials getMaterial();

    void setMaterial(ChestMaterials material);

}
