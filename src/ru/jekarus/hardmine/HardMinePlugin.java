package ru.jekarus.hardmine;

import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.HardMineApi;
import ru.jekarus.hardmine.impl.listener.HmcListeners;
import ru.jekarus.structures.bed.BedStructureType;
import ru.jekarus.structures.bed.BedTypes;
import ru.jekarus.structures.chest.basic.BasicChestStructureType;
import ru.jekarus.structures.chest.basic.BasicChestTypes;
import ru.jekarus.structures.chest.dual.DoubleChestStructureType;
import ru.jekarus.structures.chest.ender.EnderChestStructureType;
import ru.jekarus.structures.crafting.table.basic.BasicCraftingTableStructureType;
import ru.jekarus.structures.crafting.table.basic.BasicCraftingTableTypes;
import ru.jekarus.structures.enchanting.table.EnchantingTableStructureType;
import ru.jekarus.structures.furnace.advanced.AdvancedFurnaceStructureType;
import ru.jekarus.structures.furnace.auto.AutoFurnaceStructureType;
import ru.jekarus.structures.furnace.basic.BasicFurnaceStructureType;

public class HardMinePlugin extends JavaPlugin {

    public static final String PLUGIN_NAME = "HardMine";
    public static final String VERSION = "1.0-BETA";

    private HmcApi api;
    private HmcListeners listeners;

    @Override
    public void onLoad()
    {
        this.api = new HmcApi();
        this.listeners = new HmcListeners(this.api);
    }

    @Override
    public void onEnable()
    {

        for (BasicCraftingTableTypes type : BasicCraftingTableTypes.values())
        {
            this.api.getStructureTypeContainer().register(new BasicCraftingTableStructureType(this, this.api, type));
        }

        for (BasicChestTypes type : BasicChestTypes.values())
        {
            this.api.getStructureTypeContainer().register(new BasicChestStructureType(this, type));
        }

        for (BedTypes type : BedTypes.values())
        {
            this.api.getStructureTypeContainer().register(new BedStructureType(this, type));
        }

//        this.api.getStructureTypeContainer().register(new BedStructureType(this, BedTypes.SPRUCE));

        this.api.getStructureTypeContainer().register(new AdvancedFurnaceStructureType(this));
        this.api.getStructureTypeContainer().register(new EnderChestStructureType(this));
        this.api.getStructureTypeContainer().register(new DoubleChestStructureType(this));
        this.api.getStructureTypeContainer().register(new EnchantingTableStructureType(this));
        this.api.getStructureTypeContainer().register(new BasicFurnaceStructureType(this));
        this.api.getStructureTypeContainer().register(new AutoFurnaceStructureType(this));
        this.listeners.register(this);
    }

    @Override
    public void onDisable()
    {
        this.api.disable();
        this.listeners.unregister();
    }

    public HardMineApi getApi()
    {
        return this.api;
    }

}
