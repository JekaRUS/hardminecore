package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Furnace;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockFurnace;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockFaces;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.schematic.block.utils.FurnaceMaterials;

public class HmcSchematicBlockFurnace extends HmcSchematicBlockFaced<SchematicBlockFurnace> implements SchematicBlockFurnace {

    protected FurnaceMaterials material;

    public HmcSchematicBlockFurnace()
    {
        this(FurnaceMaterials.ANY, BlockFaces.ANY);
    }

    public HmcSchematicBlockFurnace(FurnaceMaterials material, BlockFaces face)
    {
        super(face);
        this.material = material;
    }

    @Override
    public FurnaceMaterials getMaterial()
    {
        return this.material;
    }

    @Override
    public void setMaterial(FurnaceMaterials material)
    {
        this.material = material;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return this.material.asBukkit();
    }

    @Override
    public void place(Block block)
    {
        block.setType(this.material.asBukkit());
        Furnace furnace = this.getBukkitFurnace(block);
        furnace.setFacingDirection(this.face.asBukkit());
    }

    @Override
    public boolean compare(Block block)
    {
        if (!this.material.equalsType(block.getType()))
        {
            return false;
        }
        Furnace furnace = this.getBukkitFurnace(block);
        if (!this.face.equalsFace(furnace.getFacing()))
        {
            return false;
        }
        return true;
    }

    private Furnace getBukkitFurnace(Block block)
    {
        return (Furnace) block.getState().getData();
    }

    @Override
    public SchematicBlockFurnace cloneBlock()
    {
        return new HmcSchematicBlockFurnace(this.material, this.face);
    }

    @Override
    public SchematicBlockFurnace cloneBlock(BlockRotate rotate)
    {
        if (this.face == BlockFaces.ANY)
        {
            return this.cloneBlock();
        }
        else
        {
            return new HmcSchematicBlockFurnace(this.material, BlockFaces.rotate(this.face, rotate));
        }
    }

}
