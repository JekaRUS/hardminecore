package ru.jekarus.hardmine.api.schematic.block.utils;

public enum BlockRotate {

    R_0,
    R_90,
    R_180,
    R_270,
    R_360

}
