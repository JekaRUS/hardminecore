package ru.jekarus.hardmine.api.schematic.block.utils;

import org.bukkit.Material;

public enum ChestMaterials {

    ANY(Material.CHEST)
            {
                @Override
                public boolean equalsType(Material type)
                {
                    return type == Material.CHEST || type == Material.TRAPPED_CHEST;
                }
            },
    NORMAL(Material.CHEST),
    TRAPPED(Material.TRAPPED_CHEST);

    private final Material type;

    ChestMaterials(Material type)
    {
        this.type = type;
    }

    public Material asBukkit()
    {
        return this.type;
    }

    public boolean equalsType(Material type)
    {
        return this.type == type;
    }

}
