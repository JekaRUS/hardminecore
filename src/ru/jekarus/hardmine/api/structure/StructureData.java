package ru.jekarus.hardmine.api.structure;

import org.bukkit.block.Block;
import ru.jekarus.hardmine.impl.structure.HmcStructureData;

public interface StructureData extends StructureSerializer, StructureDeserializer {

    StructureLocation getLocation();

    static StructureData of(Block block, int sizeX, int sizeZ, long createdTime)
    {
        return new HmcStructureData(
                StructureLocation.of(block, sizeX, sizeZ),
                createdTime
        );
    }

    long getCreateTime();

}
