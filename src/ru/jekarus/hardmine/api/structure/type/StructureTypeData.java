package ru.jekarus.hardmine.api.structure.type;

import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.impl.structure.HmcStructureTypeData;

public interface StructureTypeData {

    static StructureTypeData of(JavaPlugin owner, String title)
    {
        if (owner == null || title == null)
        {
            throw new NullPointerException();
        }
        return new HmcStructureTypeData(owner, title);
    }

    JavaPlugin getOwner();

    StructureTypeTitle getTitle();

}
