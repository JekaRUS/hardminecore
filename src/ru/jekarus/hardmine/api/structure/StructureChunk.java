package ru.jekarus.hardmine.api.structure;

import org.bukkit.Chunk;
import ru.jekarus.hardmine.impl.structure.HmcStructureChunk;

public interface StructureChunk extends StructureSerializer, StructureDeserializer {

    int getX();

    int getZ();

    static StructureChunk of(StructureChunk chunk)
    {
        return of(chunk.getX(), chunk.getZ());
    }

    static StructureChunk of(Chunk chunk)
    {
        return of(chunk.getX(), chunk.getZ());
    }

    static StructureChunk of(int x, int z)
    {
        return new HmcStructureChunk(x, z);
    }

}
