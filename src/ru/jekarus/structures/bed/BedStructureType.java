package ru.jekarus.structures.bed;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.material.Bed;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.schematic.RotatableSchematic;
import ru.jekarus.hardmine.api.schematic.RotatableSchematicBuilder;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockBed;
import ru.jekarus.hardmine.api.schematic.block.utils.BedParts;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockColors;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockFaces;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.RotatableStructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;
import ru.jekarus.structures.furnace.auto.AutoFurnaceStructure;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public class BedStructureType implements RotatableStructureType {

    private final StructureTypeData data;
    private final StructureSize size;

    private final Collection<SchematicBlock> interactedBlocks;

    private final RotatableSchematic schematic;

    public BedStructureType(JavaPlugin plugin, BedTypes type)
    {
        this.data = StructureTypeData.of(plugin, "bed_" + type.getName());
        this.size = StructureSize.of(4, 3, 3);

        this.interactedBlocks = Arrays.asList(
                SchematicBlock.bed(BedParts.TOP, BlockColors.ANY, BlockFaces.EAST),
                SchematicBlock.bed(BedParts.TOP, BlockColors.ANY, BlockFaces.SOUTH),
                SchematicBlock.bed(BedParts.TOP, BlockColors.ANY, BlockFaces.WEST),
                SchematicBlock.bed(BedParts.TOP, BlockColors.ANY, BlockFaces.NORTH),
                SchematicBlock.bed(BedParts.BOTTOM, BlockColors.ANY, BlockFaces.EAST),
                SchematicBlock.bed(BedParts.BOTTOM, BlockColors.ANY, BlockFaces.SOUTH),
                SchematicBlock.bed(BedParts.BOTTOM, BlockColors.ANY, BlockFaces.WEST),
                SchematicBlock.bed(BedParts.BOTTOM, BlockColors.ANY, BlockFaces.NORTH)
        );

        RotatableSchematicBuilder builder = RotatableSchematic.builder(this);
        builder
                .shape(0, "abba", "cdde", "affa")
                .shape(1, "bggh", "cijg", "fggh")
                .shape(2, "gggg", "kggg", "gggg");
        builder
                .setBlock('a', type.getAngle())
                .setBlock('b', type.getNorth())
                .setBlock('c', SchematicBlock.of(Material.BOOKSHELF))
                .setBlock('d', SchematicBlock.wool())
                .setBlock('e', type.getEast())
                .setBlock('f', type.getSouth())
                .setBlock('g', SchematicBlock.ban(SchematicBlock.bed()))
                .setBlock('h', SchematicBlock.of(Material.TORCH))
                .setBlock('i', SchematicBlock.bed(BedParts.TOP, BlockColors.ANY, BlockFaces.WEST))
                .setBlock('j', SchematicBlock.bed(BedParts.BOTTOM, BlockColors.ANY, BlockFaces.WEST))
                .setBlock('k', type.getSlab());
        this.schematic = (RotatableSchematic) builder.build();
    }

    @Override
    public Collection<BlockRotate> getRotates(SchematicBlock schematicBlock)
    {
        SchematicBlockBed bed = (SchematicBlockBed) schematicBlock;
        switch (bed.getFace())
        {
            case WEST:
                return Collections.singletonList(BlockRotate.R_0);
            case NORTH:
                return Collections.singletonList(BlockRotate.R_90);
            case EAST:
                return Collections.singletonList(BlockRotate.R_180);
            case SOUTH:
                return Collections.singletonList(BlockRotate.R_270);
            default:
                return Arrays.asList(BlockRotate.R_0, BlockRotate.R_90, BlockRotate.R_180, BlockRotate.R_270);
        }
    }

    @Override
    public Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player, BlockRotate rotate)
    {
        SchematicBlockBed bed = (SchematicBlockBed) schematicBlock;

        Bed data = (Bed) block.getState().getData();
        System.out.println(data.getFacing());


        Block minBlock = this.getMinBlock(block, bed, rotate);
        if (this.schematic.checkStructure(minBlock, rotate))
        {
            return Optional.of(new AutoFurnaceStructure(
                    this, StructureUser.of(player), StructureAccess.of(),
                    StructureData.of(
                            minBlock, this.size.getX(), this.size.getZ(), System.currentTimeMillis()
                    )
            ));
        }
        return Optional.empty();
    }

    private Block getMinBlock(Block block, SchematicBlockBed bed, BlockRotate rotate)
    {
        switch (rotate)
        {
            case R_90:
                return bed.getPart() == BedParts.TOP
                        ? block.getRelative(-1, -1, -1)
                        : block.getRelative(-1, -1, -2);
            case R_180:
                return bed.getPart() == BedParts.TOP
                        ? block.getRelative(-2, -1, -1)
                        : block.getRelative(-1, -1, -1);
            case R_270:
                return bed.getPart() == BedParts.TOP
                        ? block.getRelative(-1, -1, -2)
                        : block.getRelative(-1, -1, -1);
            default:
                return bed.getPart() == BedParts.TOP
                        ? block.getRelative(-1, -1, -1)
                        : block.getRelative(-2, -1, -1);
        }
    }

    @Override
    public StructureTypeData getData()
    {
        return this.data;
    }

    @Override
    public StructureSize getSize()
    {
        return this.size;
    }

    @Override
    public Structure createEmpty()
    {
        return new AutoFurnaceStructure(this);
    }

    @Override
    public Collection<SchematicBlock> getInteractedBlocks()
    {
        return this.interactedBlocks;
    }
}