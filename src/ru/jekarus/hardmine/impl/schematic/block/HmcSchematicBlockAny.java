package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockAny;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;

public class HmcSchematicBlockAny implements SchematicBlockAny {

    @Override
    public Material asBukkitMaterial()
    {
        return Material.STONE;
    }

    @Override
    public void place(Block block)
    {
        block.setType(Material.STONE);
    }

    @Override
    public boolean compare(Block block)
    {
        return true;
    }

    @Override
    public SchematicBlockAny cloneBlock()
    {
        return new HmcSchematicBlockAny();
    }

    @Override
    public SchematicBlockAny cloneBlock(BlockRotate rotate)
    {
        return this.cloneBlock();
    }

}
