package ru.jekarus.structures.bed;

import org.bukkit.Material;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.utils.*;

public enum BedTypes {

    OAK(
            "oak",
            SchematicBlock.wood(WoodMaterials.OAK, WoodFaces.UP),
            SchematicBlock.slab(SlabMaterials.OAK, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.OAK, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.OAK, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.OAK, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    SPRUCE(
            "spruce",
            SchematicBlock.wood(WoodMaterials.SPRUCE, WoodFaces.UP),
            SchematicBlock.slab(SlabMaterials.SPRUCE, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.SPRUCE, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.SPRUCE, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.SPRUCE, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    BIRCH(
            "birch",
            SchematicBlock.wood(WoodMaterials.BIRCH, WoodFaces.UP),
            SchematicBlock.slab(SlabMaterials.BIRCH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.BIRCH, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.BIRCH, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.BIRCH, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    JUNGLE(
            "jungle",
            SchematicBlock.wood(WoodMaterials.JUNGLE, WoodFaces.UP),
            SchematicBlock.slab(SlabMaterials.JUNGLE, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.JUNGLE, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.JUNGLE, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.JUNGLE, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    ACACIA(
            "acacia",
            SchematicBlock.wood(WoodMaterials.ACACIA, WoodFaces.UP),
            SchematicBlock.slab(SlabMaterials.ACACIA, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.ACACIA, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.ACACIA, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.ACACIA, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    DARK_OAK(
            "dark_oak",
            SchematicBlock.wood(WoodMaterials.DARK_OAK, WoodFaces.UP),
            SchematicBlock.slab(SlabMaterials.DARK_OAK, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.DARK_OAK, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.DARK_OAK, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.DARK_OAK, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    QUARTZ(
            "quartz",
            SchematicBlock.of(Material.QUARTZ_BLOCK),
            SchematicBlock.slab(SlabMaterials.QUARTZ, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.QUARTZ, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.QUARTZ, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.QUARTZ, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    NETHER(
            "nether",
            SchematicBlock.of(Material.NETHER_WART_BLOCK),
            SchematicBlock.slab(SlabMaterials.NETHER, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.NETHER, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.NETHER, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.NETHER, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    SMOOTH(
            "smooth",
            SchematicBlock.of(Material.SMOOTH_BRICK),
            SchematicBlock.slab(SlabMaterials.SMOOTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    RED_SANDSTONE(
            "red_sandstone",
            SchematicBlock.of(Material.RED_SANDSTONE),
            SchematicBlock.slab(SlabMaterials.RED_SANDSTONE, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.RED_SANDSTONE, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.RED_SANDSTONE, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.RED_SANDSTONE, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    COBBLESTONE(
            "cobblestone",
            SchematicBlock.of(Material.COBBLESTONE),
            SchematicBlock.slab(SlabMaterials.COBBLESTONE, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    BRICK(
            "brick",
            SchematicBlock.of(Material.BRICK),
            SchematicBlock.slab(SlabMaterials.BRICK, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.BRICK, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.BRICK, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.BRICK, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    SANDSTONE(
            "sandstone",
            SchematicBlock.of(Material.SANDSTONE),
            SchematicBlock.slab(SlabMaterials.SANDSTONE, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.SANDSTONE, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.SANDSTONE, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.SANDSTONE, BlockFaces.EAST, BlockPositions.BOTTOM)
    ),
    PURPUR(
            "purpur",
            SchematicBlock.of(Material.PURPUR_PILLAR),
            SchematicBlock.slab(SlabMaterials.PURPUR, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.PURPUR, BlockFaces.NORTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.PURPUR, BlockFaces.SOUTH, BlockPositions.BOTTOM),
            SchematicBlock.stairs(StairsMaterials.PURPUR, BlockFaces.EAST, BlockPositions.BOTTOM)
    );

    private final String name;
    private final SchematicBlock angle;
    private final SchematicBlock slab;
    private final SchematicBlock north;
    private final SchematicBlock south;
    private final SchematicBlock east;

    BedTypes(String name, SchematicBlock angle, SchematicBlock slab, SchematicBlock north, SchematicBlock south, SchematicBlock east)
    {
        this.name = name;
        this.angle = angle;
        this.slab = slab;
        this.north = north;
        this.south = south;
        this.east = east;
    }

    public String getName()
    {
        return this.name;
    }

    public SchematicBlock getAngle()
    {
        return this.angle;
    }

    public SchematicBlock getSlab()
    {
        return this.slab;
    }

    public SchematicBlock getNorth()
    {
        return this.north;
    }

    public SchematicBlock getSouth()
    {
        return this.south;
    }

    public SchematicBlock getEast()
    {
        return this.east;
    }

}
