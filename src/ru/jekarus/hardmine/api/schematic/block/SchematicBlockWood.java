package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.WoodFaces;
import ru.jekarus.hardmine.api.schematic.block.utils.WoodMaterials;

public interface SchematicBlockWood extends SchematicBlock<SchematicBlockWood> {

    WoodMaterials getMaterial();

    void setMaterial(WoodMaterials material);

    WoodFaces getFace();

    void setFace(WoodFaces face);

}
