package ru.jekarus.hardmine.impl.structure.access;

import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccessUser;
import ru.jekarus.hardmine.impl.structure.HmcStructureUser;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class HmcStructureAccessUser implements StructureAccessUser {

    private static int save_version = 0;

    private StructureUser user;
    private boolean canOpen = false;
    private boolean canOpenSettings = false;
    private boolean canPlace = false;
    private boolean canBreak = false;

    public HmcStructureAccessUser(StructureUser user)
    {
        this.user = user;
    }

    public HmcStructureAccessUser()
    {

    }

    @Override
    public StructureUser getUser()
    {
        return this.user;
    }

    @Override
    public boolean canOpen()
    {
        return this.canOpen;
    }

    @Override
    public boolean canOpenSettings()
    {
        return this.canOpenSettings;
    }

    @Override
    public boolean canPlace()
    {
        return this.canPlace;
    }

    @Override
    public boolean canBreak()
    {
        return this.canPlace;
    }

    @Override
    public void setCanOpen(boolean value)
    {
        this.canOpen = value;
    }

    @Override
    public void setCanOpenSettings(boolean value)
    {
        this.canOpenSettings = value;
    }

    @Override
    public void setCanPlace(boolean value)
    {
        this.canPlace = value;
    }

    @Override
    public void setCanBreak(boolean value)
    {
        this.canBreak = value;
    }

    @Override
    public void read(DataInputStream stream) throws IOException
    {
        int save_version = stream.readInt();

        this.user = new HmcStructureUser() {{
            this.read(stream);
        }};

        this.canOpen = stream.readBoolean();
        this.canOpenSettings = stream.readBoolean();
        this.canPlace = stream.readBoolean();
        this.canBreak = stream.readBoolean();
    }

    @Override
    public void write(DataOutputStream stream) throws IOException
    {
        stream.writeInt(save_version);

        this.user.write(stream);
        stream.writeBoolean(this.canOpen);
        stream.writeBoolean(this.canOpenSettings);
        stream.writeBoolean(this.canPlace);
        stream.writeBoolean(this.canBreak);
    }
}
