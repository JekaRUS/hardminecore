package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.BlockColors;

public interface SchematicBlockWool extends SchematicBlock<SchematicBlockWool> {

    BlockColors getColor();

    void setColor(BlockColors color);

}
