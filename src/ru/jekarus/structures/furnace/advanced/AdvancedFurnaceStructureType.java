package ru.jekarus.structures.furnace.advanced;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.schematic.RotatableSchematic;
import ru.jekarus.hardmine.api.schematic.RotatableSchematicBuilder;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockFurnace;
import ru.jekarus.hardmine.api.schematic.block.utils.*;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.RotatableStructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public class AdvancedFurnaceStructureType implements RotatableStructureType {

    private final StructureTypeData data;
    private final StructureSize size;

    private final Collection<SchematicBlock> interactedBlocks;

    private final RotatableSchematic schematic;

    public AdvancedFurnaceStructureType(JavaPlugin plugin)
    {
        this.data = StructureTypeData.of(plugin, "advanced_furnace");
        this.size = StructureSize.of(3, 3, 9);

        this.interactedBlocks = Arrays.asList(
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.EAST),
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.SOUTH),
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.WEST),
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.NORTH)
        );

        RotatableSchematicBuilder builder = RotatableSchematic.builder(this);
        builder
                .shape(0, "aaa", "aab", "aac", "aab", "dae", "aab", "aac", "aab", "aaa")
                .shape(1, "dfd", "agh", "aie", "agh", "jik", "agh", "aie", "agh", "drd")
                .shape(2, "ala", "amn", "opq", "abn", "dnd", "abn", "opq", "aln", "ama");
        builder
                .setBlock('a', SchematicBlock.of(Material.SMOOTH_BRICK))
                .setBlock('b', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.EAST, BlockPositions.BOTTOM))
                .setBlock('c', SchematicBlock.of(Material.COBBLESTONE))
                .setBlock('d', SchematicBlock.of(Material.COBBLE_WALL))
                .setBlock('e', SchematicBlock.chest())
                .setBlock('f', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.NORTH, BlockPositions.TOP))
                .setBlock('g', SchematicBlock.of(Material.REDSTONE_BLOCK))
                .setBlock('h', SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.EAST))
                .setBlock('i', SchematicBlock.of(Material.IRON_BLOCK))
                .setBlock('j', SchematicBlock.of(Material.DIAMOND_BLOCK))
                .setBlock('k', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.EAST, BlockPositions.TOP))

                .setBlock('l', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.NORTH, BlockPositions.BOTTOM))
                .setBlock('m', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.SOUTH, BlockPositions.BOTTOM))
                .setBlock('n', SchematicBlock.slab(SlabMaterials.SMOOTH, BlockPositions.BOTTOM))
                .setBlock('o', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.WEST, BlockPositions.BOTTOM))
                .setBlock('p', SchematicBlock.slab(SlabMaterials.COBBLESTONE, BlockPositions.BOTTOM))
                .setBlock('q', SchematicBlock.ANY)
                .setBlock('r', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.SOUTH, BlockPositions.TOP));
        this.schematic = (RotatableSchematic) builder.build();
    }

    @Override
    public Collection<BlockRotate> getRotates(SchematicBlock schematicBlock)
    {
        SchematicBlockFurnace furnace = (SchematicBlockFurnace) schematicBlock;
        switch (furnace.getFace())
        {
            case EAST:
                return Collections.singletonList(BlockRotate.R_0);
            case SOUTH:
                return Collections.singletonList(BlockRotate.R_90);
            case WEST:
                return Collections.singletonList(BlockRotate.R_180);
            case NORTH:
                return Collections.singletonList(BlockRotate.R_270);
            default:
                return Arrays.asList(BlockRotate.R_0, BlockRotate.R_90, BlockRotate.R_180, BlockRotate.R_270);
        }
    }

    @Override
    public Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player, BlockRotate rotate)
    {
        SchematicBlockFurnace furnace = (SchematicBlockFurnace) schematicBlock;
        Block minBlock = this.getMinBlock(block, rotate, furnace);

        if (minBlock == null)
        {
            return Optional.empty();
        }

        Bukkit.broadcastMessage(String.format("%d %d %d", block.getX(), block.getY(), block.getZ()));

        if (this.schematic.checkStructure(minBlock, rotate))
        {
            return Optional.of(new AdvancedFurnaceStructure(
                    this, StructureUser.of(player), StructureAccess.of(),
                    StructureData.of(
                            minBlock, this.size.getX(), this.size.getZ(), System.currentTimeMillis()
                    )
            ));
        }
        return Optional.empty();
    }

    private Block getMinBlock(Block block, BlockRotate rotate, SchematicBlockFurnace furnace)
    {
        Block relative;
        switch (rotate)
        {
            case R_0:
                relative = this.findFurnace(block, furnace, 0, -1);
                return relative == null ? null : relative.getRelative(-2, -1, -1);
            case R_90:
                relative = this.findFurnace(block, furnace, -1, 0);
                return relative == null ? null : relative.getRelative(-1, -1, -2);
            case R_180:
                relative = this.findFurnace(block, furnace, 0, -1);
                return relative == null ? null : relative.getRelative(0, -1, -1);
            case R_270:
                relative = this.findFurnace(block, furnace, -1, 0);
                return relative == null ? null : relative.getRelative(-1, -1, 0);
        }
        return null;
    }

    private Block findFurnace(Block block, SchematicBlockFurnace furnace, int dx, int dz)
    {
        Block relative;
        for (int i = 0; i < 4; i++)
        {
            relative = block.getRelative(2 * i * dx, 0, 2 * i * dz);
            if (furnace.compare(relative) && relative.getRelative(dx, 0, dz).getType() == Material.COBBLE_WALL)
            {
                return relative;
            }
        }
        return null;
    }

    @Override
    public StructureTypeData getData()
    {
        return this.data;
    }

    @Override
    public StructureSize getSize()
    {
        return this.size;
    }

    @Override
    public Structure createEmpty()
    {
        return new AdvancedFurnaceStructure(this);
    }

    @Override
    public Collection<SchematicBlock> getInteractedBlocks()
    {
        return this.interactedBlocks;
    }
}
