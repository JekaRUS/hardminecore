package ru.jekarus.hardmine.api;

import ru.jekarus.hardmine.api.exception.StructureAlreadyRegisteredException;
import ru.jekarus.hardmine.api.structure.type.StructureType;

public interface StructureTypeContainer {

    void register(StructureType type) throws StructureAlreadyRegisteredException;

    void unregister(StructureType type);

}
