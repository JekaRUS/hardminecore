package ru.jekarus.hardmine.impl.structure.access;

import com.sun.istack.internal.NotNull;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.access.StructureAccessUser;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;

public class HmcStructureAccess implements StructureAccess {

    private static int save_version = 0;

    private Deque<StructureAccessUser> users;

    public HmcStructureAccess()
    {
        this.users = new LinkedList<>();
    }

    public HmcStructureAccess(Collection<StructureAccessUser> users)
    {
        this.users = new LinkedList<>(users);
    }

    @Override
    public void add(@NotNull StructureAccessUser user)
    {
        this.users.add(user);
    }

    @Override
    public void remove(@NotNull StructureAccessUser user)
    {
        this.users.remove(user);
    }

    @NotNull
    @Override
    public Collection<StructureAccessUser> asCollection()
    {
        return this.users;
    }

    @Override
    public void read(DataInputStream stream) throws IOException
    {
        int save_version = stream.readInt();

        for (int i = 0; i < stream.readInt(); i++)
        {
            this.users.addLast(
                    new HmcStructureAccessUser() {{
                        this.read(stream);
                    }}
            );
        }
    }

    @Override
    public void write(DataOutputStream stream) throws IOException
    {
        stream.writeInt(save_version);

        stream.writeInt(this.users.size());
        for (StructureAccessUser user : this.users)
        {
            user.write(stream);
        }
    }

}
