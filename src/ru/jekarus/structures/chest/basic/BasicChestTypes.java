package ru.jekarus.structures.chest.basic;

import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.utils.PlanksMaterials;
import ru.jekarus.hardmine.api.schematic.block.utils.WoodFaces;
import ru.jekarus.hardmine.api.schematic.block.utils.WoodMaterials;

public enum BasicChestTypes {

    OAK(
            "oak",
            SchematicBlock.wood(WoodMaterials.OAK, WoodFaces.UP),
            SchematicBlock.wood(WoodMaterials.OAK, WoodFaces.WEST),
            SchematicBlock.wood(WoodMaterials.OAK, WoodFaces.NORTH),
            SchematicBlock.plank(PlanksMaterials.OAK)
    ),
    SPRUCE(
            "spruce",
            SchematicBlock.wood(WoodMaterials.SPRUCE, WoodFaces.UP),
            SchematicBlock.wood(WoodMaterials.SPRUCE, WoodFaces.WEST),
            SchematicBlock.wood(WoodMaterials.SPRUCE, WoodFaces.NORTH),
            SchematicBlock.plank(PlanksMaterials.SPRUCE)
    ),
    BIRCH(
            "birch",
            SchematicBlock.wood(WoodMaterials.BIRCH, WoodFaces.UP),
            SchematicBlock.wood(WoodMaterials.BIRCH, WoodFaces.WEST),
            SchematicBlock.wood(WoodMaterials.BIRCH, WoodFaces.NORTH),
            SchematicBlock.plank(PlanksMaterials.BIRCH)
    ),
    JUNGLE(
            "jungle",
            SchematicBlock.wood(WoodMaterials.JUNGLE, WoodFaces.UP),
            SchematicBlock.wood(WoodMaterials.JUNGLE, WoodFaces.WEST),
            SchematicBlock.wood(WoodMaterials.JUNGLE, WoodFaces.NORTH),
            SchematicBlock.plank(PlanksMaterials.JUNGLE)
    ),
    ACACIA(
            "acacia",
            SchematicBlock.wood(WoodMaterials.ACACIA, WoodFaces.UP),
            SchematicBlock.wood(WoodMaterials.ACACIA, WoodFaces.WEST),
            SchematicBlock.wood(WoodMaterials.ACACIA, WoodFaces.NORTH),
            SchematicBlock.plank(PlanksMaterials.ACACIA)
    ),
    DARK_OAK(
            "dark_oak",
            SchematicBlock.wood(WoodMaterials.DARK_OAK, WoodFaces.UP),
            SchematicBlock.wood(WoodMaterials.DARK_OAK, WoodFaces.WEST),
            SchematicBlock.wood(WoodMaterials.DARK_OAK, WoodFaces.NORTH),
            SchematicBlock.plank(PlanksMaterials.DARK_OAK)
    );

    private final String name;
    private final SchematicBlock top;
    private final SchematicBlock west;
    private final SchematicBlock north;
    private final SchematicBlock planks;

    BasicChestTypes(String name, SchematicBlock top, SchematicBlock west, SchematicBlock north, SchematicBlock planks)
    {
        this.name = name;
        this.top = top;
        this.west = west;
        this.north = north;
        this.planks = planks;
    }

    public String getName()
    {
        return this.name;
    }

    public SchematicBlock getTop()
    {
        return this.top;
    }

    public SchematicBlock getWest()
    {
        return this.west;
    }

    public SchematicBlock getNorth()
    {
        return this.north;
    }

    public SchematicBlock getPlanks()
    {
        return this.planks;
    }
}
