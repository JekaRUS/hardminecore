package ru.jekarus.hardmine.impl.storage;

import ru.jekarus.hardmine.api.storage.StorageStructures;
import ru.jekarus.hardmine.api.structure.*;
import ru.jekarus.hardmine.api.structure.type.StructureType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public class HmcStorageStructures implements StorageStructures {

    private StructureType type;
    private Collection<StructureUniqueId> structures;

    public HmcStorageStructures(StructureType type)
    {
        this.type = type;
        this.structures = new ArrayList<>();
    }

    @Override
    public void register(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureUniqueId uniqueId = structure.getUniqueId();
        if (uniqueId == null)
        {
            throw new NullPointerException();
        }

        this.structures.add(uniqueId);
    }

    @Override
    public void remove(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureUniqueId uniqueId = structure.getUniqueId();
        if (uniqueId == null)
        {
            throw new NullPointerException();
        }
        this.structures.remove(uniqueId);
    }

    @Override
    public boolean contains(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureUniqueId uniqueId = structure.getUniqueId();
        if (uniqueId == null)
        {
            throw new NullPointerException();
        }
        for (StructureUniqueId structureUniqueId : this.structures)
        {
            if (structureUniqueId.equals(uniqueId))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public Optional<Structure> getStructure(int x, int y, int z)
    {
        Structure structure;
        StructureData data;
        StructureSize size;
        StructureLocation location;

        for (StructureUniqueId uniqueId : this.structures)
        {
            structure = uniqueId.getStructure();
            if (structure != null)
            {
                data = uniqueId.getStructure().getData();
                size = structure.getType().getSize();
                if (data != null && size != null)
                {
                    location = data.getLocation();
                    if (location != null)
                    {
                        if (x < location.getX() + size.getX() && location.getX() <= x && y < location.getY() + size.getY() && location.getY() <= y && z < location.getZ() + size.getZ() && location.getZ() <= z)
                        {
                            return Optional.of(structure);
                        }
                    }
                }
            }
        }
        return Optional.empty();
    }
}