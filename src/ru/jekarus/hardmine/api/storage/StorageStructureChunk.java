package ru.jekarus.hardmine.api.storage;

import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureChunk;
import ru.jekarus.hardmine.impl.storage.HmcStorageStructureChunk;

import java.util.Optional;

public interface StorageStructureChunk {

    static StorageStructureChunk of(StructureChunk structureChunk)
    {
        return new HmcStorageStructureChunk(structureChunk);
    }

    void register(Structure structure);

    void remove(Structure structure);

    boolean contains(Structure structure);

    Optional<Structure> getStructure(int x, int y, int z);

}
