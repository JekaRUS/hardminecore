package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Chest;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockChest;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockFaces;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.schematic.block.utils.ChestMaterials;

public class HmcSchematicBlockChest extends HmcSchematicBlockFaced<SchematicBlockChest> implements SchematicBlockChest {

    protected ChestMaterials material;

    public HmcSchematicBlockChest()
    {
        this(ChestMaterials.ANY, BlockFaces.ANY);
    }

    public HmcSchematicBlockChest(ChestMaterials material, BlockFaces face)
    {
        super(face);
        this.material = material;
    }

    @Override
    public ChestMaterials getMaterial()
    {
        return this.material;
    }

    @Override
    public void setMaterial(ChestMaterials material)
    {
        this.material = material;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return this.material.asBukkit();
    }

    @Override
    public void place(Block block)
    {
        block.setType(this.material.asBukkit());
        Chest chest = this.getBukkitChest(block);
        chest.setFacingDirection(this.face.asBukkit());
    }

    @Override
    public boolean compare(Block block)
    {
        if (!this.material.equalsType(block.getType()))
        {
            return false;
        }
        Chest chest = this.getBukkitChest(block);
        if (!this.face.equalsFace(chest.getFacing()))
        {
            return false;
        }
        return true;
    }

    private Chest getBukkitChest(Block block)
    {
        return (Chest) block.getState().getData();
    }

    @Override
    public SchematicBlockChest cloneBlock()
    {
        return new HmcSchematicBlockChest(this.material, this.face);
    }

    @Override
    public SchematicBlockChest cloneBlock(BlockRotate rotate)
    {
        if (this.face == BlockFaces.ANY)
        {
            return this.cloneBlock();
        }
        else
        {
            return new HmcSchematicBlockChest(this.material, BlockFaces.rotate(this.face, rotate));
        }
    }
}
