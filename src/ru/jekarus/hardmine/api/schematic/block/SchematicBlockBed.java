package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.BedParts;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockColors;

public interface SchematicBlockBed extends SchematicBlockFaced<SchematicBlockBed> {

    BedParts getPart();

    void setPart(BedParts part);

    BlockColors getColor();

    void setColor(BlockColors color);

}
