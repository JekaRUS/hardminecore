package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.PlanksMaterials;

public interface SchematicBlockPlanks extends SchematicBlock<SchematicBlockPlanks> {

    PlanksMaterials getMaterial();

    void setMaterial(PlanksMaterials material);

}
