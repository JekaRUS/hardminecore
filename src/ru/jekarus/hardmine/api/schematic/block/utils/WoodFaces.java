package ru.jekarus.hardmine.api.schematic.block.utils;

import org.bukkit.block.BlockFace;

public enum WoodFaces {

    ANY(BlockFace.NORTH)
            {
                @Override
                public boolean equalsFace(BlockFace face)
                {
                    return true;
                }
            },
    NORTH(BlockFace.NORTH),
    WEST(BlockFace.WEST),
    UP(BlockFace.UP);

    private final BlockFace face;

    WoodFaces(BlockFace face)
    {
        this.face = face;
    }

    public BlockFace asBukkit()
    {
        return this.face;
    }

    public boolean equalsFace(BlockFace face)
    {
        return this.face == face;
    }

}
