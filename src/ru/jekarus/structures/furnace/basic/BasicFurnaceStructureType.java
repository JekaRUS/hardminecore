package ru.jekarus.structures.furnace.basic;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.schematic.RotatableSchematic;
import ru.jekarus.hardmine.api.schematic.RotatableSchematicBuilder;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockFurnace;
import ru.jekarus.hardmine.api.schematic.block.utils.*;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.RotatableStructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public class BasicFurnaceStructureType implements RotatableStructureType {

    private final StructureTypeData data;
    private final StructureSize size;

    private final Collection<SchematicBlock> interactedBlocks;

    private final RotatableSchematic schematic;

    public BasicFurnaceStructureType(JavaPlugin plugin)
    {
        this.data = StructureTypeData.of(plugin, "basic_furnace");
        this.size = StructureSize.of(3, 3, 3);

        this.interactedBlocks = Arrays.asList(
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.EAST),
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.SOUTH),
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.WEST),
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.NORTH)
        );

        RotatableSchematicBuilder builder = RotatableSchematic.builder(this);
        builder
                .shape(0, "abb", "cad", "aee")
                .shape(1, "faf", "gah", "faf")
                .shape(2, "aii", "jak", "all");
        builder
                .setBlock('a', SchematicBlock.of(Material.COBBLESTONE))
                .setBlock('b', SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.NORTH, BlockPositions.TOP))
                .setBlock('c', SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.WEST, BlockPositions.TOP))
                .setBlock('d', SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.EAST, BlockPositions.BOTTOM))
                .setBlock('e', SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.SOUTH, BlockPositions.TOP))
                .setBlock('f', SchematicBlock.of(Material.COBBLE_WALL))
                .setBlock('g', SchematicBlock.of(Material.COAL_BLOCK))
                .setBlock('h', SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.EAST))
                .setBlock('i', SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.NORTH, BlockPositions.BOTTOM))
                .setBlock('j', SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.WEST, BlockPositions.BOTTOM))
                .setBlock('k', SchematicBlock.slab(SlabMaterials.COBBLESTONE, BlockPositions.BOTTOM))
                .setBlock('l', SchematicBlock.stairs(StairsMaterials.COBBLESTONE, BlockFaces.SOUTH, BlockPositions.BOTTOM));
        this.schematic = (RotatableSchematic) builder.build();
    }

    @Override
    public Collection<BlockRotate> getRotates(SchematicBlock schematicBlock)
    {
        SchematicBlockFurnace furnace = (SchematicBlockFurnace) schematicBlock;
        switch (furnace.getFace())
        {
            case EAST:
                return Collections.singletonList(BlockRotate.R_0);
            case SOUTH:
                return Collections.singletonList(BlockRotate.R_90);
            case WEST:
                return Collections.singletonList(BlockRotate.R_180);
            case NORTH:
                return Collections.singletonList(BlockRotate.R_270);
            default:
                return Arrays.asList(BlockRotate.R_0, BlockRotate.R_90, BlockRotate.R_180, BlockRotate.R_270);
        }
    }

    @Override
    public Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player, BlockRotate rotate)
    {
        Block minBlock = this.getMinBlock(block, rotate);
        if (this.schematic.checkStructure(minBlock, rotate))
        {
            return Optional.of(new BasicFurnaceStructure(
                    this, StructureUser.of(player), StructureAccess.of(),
                    StructureData.of(
                            minBlock, this.size.getX(), this.size.getZ(), System.currentTimeMillis()
                    )
            ));
        }
        return Optional.empty();
    }

    private Block getMinBlock(Block block, BlockRotate rotate)
    {
        switch (rotate)
        {
            case R_90:
                return block.getRelative(-1, -1, -2);
            case R_180:
                return block.getRelative(0, -1, -1);
            case R_270:
                return block.getRelative(-1, -1, 0);
            default:
                return block.getRelative(-2, -1, -1);
        }
    }

    @Override
    public StructureTypeData getData()
    {
        return this.data;
    }

    @Override
    public StructureSize getSize()
    {
        return this.size;
    }

    @Override
    public Structure createEmpty()
    {
        return new BasicFurnaceStructure(this);
    }

    @Override
    public Collection<SchematicBlock> getInteractedBlocks()
    {
        return this.interactedBlocks;
    }
}
