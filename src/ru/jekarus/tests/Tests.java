package ru.jekarus.tests;

public class Tests {

    public static void main(String[] args)
    {

        char[][] shape = {
                {
                        'a', 'b', 'b'
                },
                {
                        'c', 'a', 'd'
                },
                {
                        'a', 'e', 'e'
                }
        };


        int sizeX = 3;
        int sizeZ = 3;

        char[][] rotated = new char[sizeZ][sizeX];

        int orgX = 0;
        int orgZ = 0;

        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                rotated[orgX][orgZ] = shape[x][z];
                orgX++;
            }
            orgX = 0;
            orgZ++;
        }

        for (int x = 0; x < rotated.length; x++)
        {
            for (int z = 0; z < rotated[x].length; z++)
            {
                System.out.print(rotated[x][z]);
            }
            System.out.print(" ");
        }


//        char[][][] mainShape = new char[2][3][3];
//
//        String[] shape = {"abb", "cad", "aee"};
//
//        shape(mainShape, 0, "abb", "cad", "aee");
//        shape(mainShape, 1, "faf", "gah", "faf");
////        shape(mainShape, 2, "aii", "jak", "all");
//
//        for (int x = 0; x < mainShape.length; x++)
//        {
//            for (int y = 0; y < mainShape[x].length; y++)
//            {
//                for (int z = 0; z < mainShape[x][y].length; z++)
//                {
//                    System.out.print(mainShape[x][y][z]);
//                }
//                System.out.print(" ");
//            }
//            System.out.println();
//        }
//
//        test1(mainShape);

    }

    public static void shape(char[][][] mainShape, int dy, String... shape)
    {

        /*
        *
        * [0][0][0] = a
        * [0][0][1] = b
        * [0][0][2] = b
        *
        * [1][0][0] = c
        * [1][0][1] = a
        * [1][0][2] = d
        *
        *
        * */

        for (int x = 0; x < shape.length; x++)
        {
            String s = shape[x];
            if (s == null)
            {
                continue;
            }
            if (s.length() > 3)
            {
                throw new IllegalArgumentException();
            }
            for (int z = 0; z < s.length(); z++)
            { // 333-24; 34-224; 8-921-358-7501
                mainShape[dy][x][z] = s.charAt(z);
            }
        }
    }

    public static void test1(char[][][] in)
    {

        System.out.println();
//        char[][][] in = {
//                { // x = 0
//                        { // y = 0
//                                'a', 'b', 'b'
//                        },
//                        { // y = 1
//                                'c', 'a', 'd'
//                        }
//                        ,
//                        { // y = 2
//                                'a', 'e', 'e'
//                        }
//                },
//                { // x = 1
//                        { // y = 0
//                                'f', 'a', 'f'
//                        },
//                        { // y = 1
//                                'g', 'a', 'h'
//                        }
//                        ,
//                        { // y = 2
//                                'f', 'a', 'f'
//                        }
//                },
//                { // x = 2
//                        { // y = 0
//                                'a', 'i', 'i'
//                        },
//                        { // y = 1
//                                'j', 'a', 'k'
//                        }
//                        ,
//                        { // y = 2
//                                'a', 'l', 'l'
//                        }
//                }
//        };

//        char[][][] in = { // 0 0 0 - 0 1 0 - 0 2 0 -> 0
//                {
//                        {'a', 'c', 'a'}, {'b', 'a', 'e'}, {'b', 'd', 'e'}
//                },
//                {
//                        {'f', 'g', 'f'}, {'a', 'a', 'a'}, {'f', 'g', 'f'}
//                },
//                {
//                        {'a', 'j', 'a'}, {'i', 'a', 'l'}, {'i', 'k', 'l'}
//                }
//        };


        int sizeX = 3;
        int sizeY = 2;
        int sizeZ = 3;

        char[][][] out = new char[sizeY][sizeX][sizeZ];

        int orgX = 0;
        int orgY = 0;
        int orgZ = 0;


        // 000 > 000, 010 > 001, 020 > 002
        // 001 > 010, 011 > 011, 021 > 012
        // 002 > 020, 012 > 021, 022 > 022

        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                for (int y = 0; y < sizeY; y++)
                {
//                    System.out.println(String.format("%d %d %d", out.length, out[0].length, out[0][0].length));
//                    System.out.println(String.format("%d %d %d", orgX, orgY, orgZ));
                    out[orgY][orgZ][orgX]
                            =
                            in[y][z][x];
                    orgY++;
                }
                orgY = 0;
                orgZ++;
            }
            orgZ = 0;
            orgX++;
        }

        for (int x = 0; x < out.length; x++)
        {
            for (int y = 0; y < out[x].length; y++)
            {
                for (int z = 0; z < out[x][y].length; z++)
                {
                    System.out.print(out[x][y][z]);
                }
                System.out.print(" ");
            }
            System.out.println();
        }
    }

}
