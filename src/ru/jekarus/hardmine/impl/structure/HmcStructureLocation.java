package ru.jekarus.hardmine.impl.structure;

import org.bukkit.Bukkit;
import org.bukkit.World;
import ru.jekarus.hardmine.api.structure.StructureChunk;
import ru.jekarus.hardmine.api.structure.StructureLocation;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class HmcStructureLocation implements StructureLocation {

    private static int save_version = 0;

    private World world;
    private int x;
    private int y;
    private int z;
    private Collection<StructureChunk> chunks;

    public HmcStructureLocation(
            World world,
            int x,
            int y,
            int z,
            Collection<StructureChunk> chunks
    )
    {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.chunks = chunks;
    }

    public HmcStructureLocation()
    {

    }

    @Override
    public World getWorld()
    {
        return this.world;
    }

    @Override
    public int getX()
    {
        return this.x;
    }

    @Override
    public int getY()
    {
        return this.y;
    }

    @Override
    public int getZ()
    {
        return this.z;
    }

    @Override
    public Collection<StructureChunk> getChunks()
    {
        return this.chunks;
    }

    @Override
    public void read(DataInputStream stream) throws IOException
    {
        int save_version = stream.readInt();

        this.world = Bukkit.getWorld(
                new UUID(
                        stream.readLong(),
                        stream.readLong()
                )
        );
        this.x = stream.readInt();
        this.y = stream.readInt();
        this.z = stream.readInt();

        this.chunks = new ArrayList<>();
        for (int i = 0; i < stream.readInt(); i++)
        {
            this.chunks.add(
                    new HmcStructureChunk() {{
                        this.read(stream);
                    }}
            );
        }
    }

    @Override
    public void write(DataOutputStream stream) throws IOException
    {
        stream.writeInt(save_version);

        stream.writeLong(this.world.getUID().getMostSignificantBits());
        stream.writeLong(this.world.getUID().getLeastSignificantBits());
        stream.writeInt(this.x);
        stream.writeInt(this.y);
        stream.writeInt(this.z);

        stream.writeInt(this.chunks.size());
        for (StructureChunk chunk : this.chunks)
        {
            chunk.write(stream);
        }
    }

}
