package ru.jekarus.hardmine.impl.structure;

import ru.jekarus.hardmine.api.structure.StructureChunk;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class HmcStructureChunk implements StructureChunk {

    private static int save_version;

    private int x;
    private int z;

    public HmcStructureChunk()
    {

    }

    public HmcStructureChunk(int x, int z)
    {
        this.x = x;
        this.z = z;
    }

    @Override
    public int getX()
    {
        return this.x;
    }

    @Override
    public int getZ()
    {
        return this.z;
    }

    @Override
    public final boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        HmcStructureChunk that = (HmcStructureChunk) o;

        return x == that.x && z == that.z;
    }

    @Override
    public final int hashCode()
    {
        int result = x;
        result = 31 * result + z;
        return result;
    }

    @Override
    public void read(DataInputStream stream) throws IOException
    {
        int save_version = stream.readInt();

        this.x = stream.readInt();
        this.z = stream.readInt();
    }

    @Override
    public void write(DataOutputStream stream) throws IOException
    {
        stream.writeInt(save_version);

        stream.writeInt(this.x);
        stream.writeInt(this.z);
    }

}
