package ru.jekarus.hardmine.api.storage;

import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.storage.HmcStorageStructures;

import java.util.Optional;

public interface StorageStructures {

    static StorageStructures of(StructureType type)
    {
        return new HmcStorageStructures(type);
    }

    void register(Structure structure);

    void remove(Structure structure);

    boolean contains(Structure structure);

    Optional<Structure> getStructure(int x, int y, int z);
}
