package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Stairs;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockStairs;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockFaces;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockPositions;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.schematic.block.utils.StairsMaterials;

public class HmcSchematicBlockStairs implements SchematicBlockStairs {

    private StairsMaterials material;
    private BlockFaces face;
    private BlockPositions position;

    public HmcSchematicBlockStairs()
    {
        this(StairsMaterials.ANY, BlockFaces.ANY, BlockPositions.ANY);
    }

    public HmcSchematicBlockStairs(
            StairsMaterials material, BlockFaces face, BlockPositions position
    )
    {
        this.material = material;
        this.face = face;
        this.position = position;
    }

    @Override
    public StairsMaterials getMaterial()
    {
        return this.material;
    }

    @Override
    public void setMaterial(StairsMaterials material)
    {
        this.material = material;
    }

    @Override
    public BlockFaces getFace()
    {
        return this.face;
    }

    @Override
    public void setFace(BlockFaces face)
    {
        this.face = face;
    }

    @Override
    public BlockPositions getPosition()
    {
        return this.position;
    }

    @Override
    public void setPosition(BlockPositions position)
    {
        this.position = position;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return this.material.asBukkit();
    }

    @Override
    public void place(Block block)
    {
        block.setType(this.material.asBukkit());
        Stairs stairs = this.getBukkitStairs(block);
        stairs.setFacingDirection(this.face.asBukkit());
        stairs.setInverted(this.position == BlockPositions.TOP);
    }

    @Override
    public boolean compare(Block block)
    {
        if (!this.material.equalsType(block.getType()))
        {
            Bukkit.broadcastMessage("invalid type - " + block.getType() + " - need - " + this.material.asBukkit());
            return false;
        }
        Stairs stairs = this.getBukkitStairs(block);
        if (this.position != BlockPositions.ANY)
        {
            if (stairs.isInverted() && this.position != BlockPositions.TOP || !stairs.isInverted() && this.position != BlockPositions.BOTTOM)
            {
                Bukkit.broadcastMessage("invalid position - " + stairs.isInverted() + " - need - " + this.position);
                return false;
            }
        }
        if (!this.face.equalsFace(stairs.getFacing()))
        {
            Bukkit.broadcastMessage("invalid face - " + stairs.getFacing() + " - need - " + this.face);
            return false;
        }
        return true;
    }

    private Stairs getBukkitStairs(Block block)
    {
        return (Stairs) block.getState().getData();
    }

    @Override
    public SchematicBlockStairs cloneBlock()
    {
        return new HmcSchematicBlockStairs(this.material, this.face, this.position);
    }

    @Override
    public SchematicBlockStairs cloneBlock(BlockRotate rotate)
    {
        if (this.face == BlockFaces.ANY)
        {
            return this.cloneBlock();
        }
        else
        {
            return new HmcSchematicBlockStairs(this.material, BlockFaces.rotate(this.face, rotate), this.position);
        }
    }
}