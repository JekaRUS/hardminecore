package ru.jekarus.hardmine.api.structure;

import java.io.DataOutputStream;
import java.io.IOException;

public interface StructureSerializer {

    void write(DataOutputStream stream) throws IOException;

}
