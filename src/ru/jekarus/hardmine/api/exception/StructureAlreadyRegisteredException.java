package ru.jekarus.hardmine.api.exception;

import ru.jekarus.hardmine.api.structure.type.StructureType;

public class StructureAlreadyRegisteredException extends RuntimeException {

    public StructureAlreadyRegisteredException(StructureType type)
    {
        super("StructureType \"" + type.getData().getTitle() + "\" already registered!");
    }

}
