package ru.jekarus.structures.furnace.auto;

import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.structure.HmcStructure;

public class AutoFurnaceStructure extends HmcStructure {

    public AutoFurnaceStructure(StructureType type, StructureUser owner, StructureAccess access, StructureData data)
    {
        super(type, owner, access, data);
    }

    public AutoFurnaceStructure(StructureType type)
    {
        super(type);
    }

}
