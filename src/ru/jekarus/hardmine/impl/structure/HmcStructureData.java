package ru.jekarus.hardmine.impl.structure;

import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureLocation;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class HmcStructureData implements StructureData {

    private static int save_version = 0;

    private StructureLocation location;
    private long createTime;

    public HmcStructureData(StructureLocation location, long createTime)
    {
        this.location = location;
        this.createTime = createTime;
    }

    public HmcStructureData()
    {

    }

    @Override
    public StructureLocation getLocation()
    {
        return this.location;
    }

    @Override
    public long getCreateTime()
    {
        return this.createTime;
    }

    @Override
    public void read(DataInputStream stream) throws IOException
    {
        int save_version = stream.readInt();

        this.location = new HmcStructureLocation() {{
            this.read(stream);
        }};
        this.createTime = stream.readLong();
    }

    @Override
    public void write(DataOutputStream stream) throws IOException
    {
        stream.writeInt(save_version);

        this.location.write(stream);
        stream.writeLong(this.createTime);
    }

}
