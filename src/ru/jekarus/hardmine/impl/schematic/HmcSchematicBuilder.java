package ru.jekarus.hardmine.impl.schematic;

import ru.jekarus.hardmine.api.schematic.Schematic;
import ru.jekarus.hardmine.api.schematic.SchematicBuilder;
import ru.jekarus.hardmine.api.schematic.SchematicLayout;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.type.StructureType;

import java.util.HashMap;
import java.util.Map;

public class HmcSchematicBuilder implements SchematicBuilder {

    protected final StructureType type;

    protected final char[][][] shape;
    protected final Map<Character, SchematicBlock> blocks;

    public HmcSchematicBuilder(StructureType type)
    {
        this.type = type;

        StructureSize size = type.getSize();
        this.shape = new char[size.getY()][size.getX()][size.getZ()];

        this.blocks = new HashMap<>();
    }

    @Override
    public SchematicBuilder shape(int dy, String... shape)
    {
        if (shape.length > this.type.getSize().getZ())
        {
            throw new IllegalArgumentException();
        }
        if (dy < 0 || dy >= this.type.getSize().getY())
        {
            throw new IllegalArgumentException();
        }

        for (int z = 0; z < shape.length; z++)
        {
            String s = shape[z];
            if (s == null)
            {
                continue;
            }
            if (s.length() > this.type.getSize().getX())
            {
                throw new IllegalArgumentException();
            }
            for (int x = 0; x < s.length(); x++)
            {
                this.shape[dy][x][z] = s.charAt(x);
            }
        }
        return this;
    }

    @Override
    public SchematicBuilder setBlock(char symbol, SchematicBlock block)
    {
        this.blocks.put(symbol, block);
        return this;
    }

    @Override
    public Schematic build()
    {
        SchematicLayout[] layouts = this.compileShape();
        return new HmcSchematic(this.type, layouts);
    }

    protected SchematicLayout[] compileShape()
    {
        StructureSize size = this.type.getSize();

        SchematicBlock[][] layout;
        SchematicLayout[] layouts = this.createLayouts(size.getY());

        char c;
        SchematicBlock block;
        for (int y = 0; y < size.getY(); y++)
        {
            layout = new SchematicBlock[size.getX()][size.getZ()];
            for (int x = 0; x < size.getX(); x++)
            {
                for (int z = 0; z < size.getZ(); z++)
                {
                    c = this.shape[y][x][z];
                    block = this.blocks.get(c);
                    if (block == null)
                    {
                        block = SchematicBlock.ANY;
                    }
                    layout[x][z] = block;
                }
            }
            layouts[y] = this.createLayout(layout);
        }
        return layouts;
    }

    protected SchematicLayout[] createLayouts(int size)
    {
        return new SchematicLayout[size];
    }

    protected SchematicLayout createLayout(SchematicBlock[][] layout)
    {
        return new HmcSchematicLayout(this.type, layout);
    }

}
