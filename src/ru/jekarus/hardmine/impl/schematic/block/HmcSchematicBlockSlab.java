package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Step;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockSlab;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockPositions;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.schematic.block.utils.SlabMaterials;

public class HmcSchematicBlockSlab implements SchematicBlockSlab {

    private SlabMaterials material;
    private BlockPositions position;

    public HmcSchematicBlockSlab()
    {
        this(SlabMaterials.ANY, BlockPositions.ANY);
    }

    public HmcSchematicBlockSlab(SlabMaterials material, BlockPositions position)
    {
        this.material = material;
        this.position = position;
    }

    @Override
    public SlabMaterials getMaterial()
    {
        return this.material;
    }

    @Override
    public void setMaterial(SlabMaterials material)
    {
        this.material = material;
    }

    @Override
    public BlockPositions getPosition()
    {
        return this.position;
    }

    @Override
    public void setPosition(BlockPositions position)
    {
        this.position = position;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return this.material.asBukkit();
    }

    @Override
    public void place(Block block)
    {
        block.setType(this.material.asBukkit());
        Step slab = this.asBukkitSlab(block);
        slab.setInverted(this.position == BlockPositions.TOP);
        slab.setMaterial(this.material.getMaterial());
    }

    @Override
    public boolean compare(Block block)
    {
        if (!this.material.equalsType(block.getType()))
        {
            Bukkit.broadcastMessage("invalid type - " + block.getType() + " - need - " + this.material.asBukkit());
            return false;
        }
        if (this.material == SlabMaterials.PURPUR || block.getType() == Material.WOOD_STEP)
        {
            if (this.position != BlockPositions.ANY)
            {
                // Ебаный спигот...
                boolean inInverted = (block.getData() & 8) != 0;
                if (inInverted && this.position != BlockPositions.TOP || !inInverted && this.position != BlockPositions.BOTTOM)
                {
                    Bukkit.broadcastMessage("invalid position - " + inInverted + " - need - " + this.position);
                    return false;
                }
            }
        }
        else
        {
            Step slab = this.asBukkitSlab(block);
            if (!this.material.equalsMaterial(slab.getMaterial()))
            {
                return false;
            }
            if (this.position != BlockPositions.ANY)
            {
                Bukkit.broadcastMessage(String.format("%s and %s or %s and %s", slab.isInverted(), this.position != BlockPositions.TOP, !slab.isInverted(), this.position != BlockPositions.BOTTOM));
                if (slab.isInverted() && this.position != BlockPositions.TOP || !slab.isInverted() && this.position != BlockPositions.BOTTOM)
                {
                    Bukkit.broadcastMessage("invalid position - " + slab.isInverted() + " - need - " + this.position);
                    return false;
                }
            }
        }
        return true;
    }

    private Step asBukkitSlab(Block block)
    {
        return (Step) block.getState().getData();
    }

    @Override
    public SchematicBlockSlab cloneBlock()
    {
        return new HmcSchematicBlockSlab(this.material, this.position);
    }

    @Override
    public SchematicBlockSlab cloneBlock(BlockRotate rotate)
    {
        return this.cloneBlock();
    }

}
