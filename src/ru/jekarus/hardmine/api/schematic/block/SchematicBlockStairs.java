package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.BlockFaces;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockPositions;
import ru.jekarus.hardmine.api.schematic.block.utils.StairsMaterials;

public interface SchematicBlockStairs extends SchematicBlock<SchematicBlockStairs> {

    StairsMaterials getMaterial();

    void setMaterial(StairsMaterials material);

    BlockFaces getFace();

    void setFace(BlockFaces face);

    BlockPositions getPosition();

    void setPosition(BlockPositions position);

}
