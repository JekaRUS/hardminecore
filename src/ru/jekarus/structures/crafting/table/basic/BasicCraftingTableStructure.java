package ru.jekarus.structures.crafting.table.basic;

import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.structure.HmcStructure;

public class BasicCraftingTableStructure extends HmcStructure {

    public BasicCraftingTableStructure(StructureType type, StructureUser owner, StructureAccess access, StructureData data)
    {
        super(type, owner, access, data);
    }

    public BasicCraftingTableStructure(StructureType type)
    {
        super(type);
    }

}
