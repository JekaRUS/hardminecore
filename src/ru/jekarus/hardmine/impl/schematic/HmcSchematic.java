package ru.jekarus.hardmine.impl.schematic;

import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.Schematic;
import ru.jekarus.hardmine.api.schematic.SchematicLayout;
import ru.jekarus.hardmine.api.structure.type.StructureType;

public class HmcSchematic implements Schematic {

    private final StructureType type;
    private SchematicLayout[] layout;

    HmcSchematic(SchematicLayout[] layout)
    {
        this(null, layout);
    }

    HmcSchematic(StructureType type, SchematicLayout[] layout)
    {
        this.type = type;
        this.layout = layout;
    }

    @Override
    public boolean checkStructure(Block block)
    {
        for (int y = 0; y < this.layout.length; y++)
        {
            if (!this.layout[y].checkStructure(block.getRelative(0, y, 0)))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean checkChangedBlock(Block minBlock, Block changedBlock)
    {
        return false; // FIXME: 03.01.2018
//        int dx = minBlock.getX() - changedBlock.getX();
//        int dy = minBlock.getY() - changedBlock.getY();
//        int dz = minBlock.getZ() - changedBlock.getZ();
//
//        dx = dx < 0 ? -dx : dx;
//        dy = dy < 0 ? -dy : dy;
//        dz = dz < 0 ? -dz : dz;
//
//        return this.schematic.length <= dx || this.schematic[0].length <= dy || this.schematic[0][0].length <= dz || !this.schematic[dx][dy][dz].compare(changedBlock);
    }

}
