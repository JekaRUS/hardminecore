package ru.jekarus.hardmine.api.schematic.block.utils;

import org.bukkit.block.BlockFace;

public enum BlockFaces {

    ANY(BlockFace.NORTH)
            {
                @Override
                public boolean equalsFace(BlockFace face)
                {
                    return true;
                }
            },
    NORTH(BlockFace.NORTH),
    EAST(BlockFace.EAST),
    SOUTH(BlockFace.SOUTH),
    WEST(BlockFace.WEST);

    private final BlockFace blockFace;

    BlockFaces(BlockFace blockFace)
    {
        this.blockFace = blockFace;
    }

    public static BlockFaces rotate(BlockFaces face, BlockRotate rotate)
    {
        int newRotate = rotate.ordinal() + face.ordinal();

        if (newRotate > 4)
        {
            newRotate = 1 + newRotate - BlockFaces.values().length;
        }

        return BlockFaces.values()[newRotate];
    }

    public boolean equalsFace(BlockFace face)
    {
        return this.blockFace == face;
    }

    public BlockFace asBukkit()
    {
        return this.blockFace;
    }
}
