package ru.jekarus.hardmine.api.structure;

import org.bukkit.entity.Player;
import ru.jekarus.hardmine.impl.structure.HmcStructureUser;

import java.util.Optional;
import java.util.UUID;

public interface StructureUser extends StructureSerializer, StructureDeserializer {

    Optional<String> getName();

    Optional<UUID> getUniqueId();

    static StructureUser of(Player player)
    {
        if (player == null)
        {
            throw new NullPointerException();
        }
        return new HmcStructureUser(player.getName(), player.getUniqueId());
    }

    static StructureUser of(String name, UUID uniqueId)
    {
        if (name == null || uniqueId == null)
        {
            throw new NullPointerException();
        }
        return new HmcStructureUser(name, uniqueId);
    }
}
