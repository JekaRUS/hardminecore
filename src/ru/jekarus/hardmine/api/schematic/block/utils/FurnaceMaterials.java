package ru.jekarus.hardmine.api.schematic.block.utils;

import org.bukkit.Material;

public enum FurnaceMaterials {

    ANY(Material.FURNACE)
            {
                @Override
                public boolean equalsType(Material type)
                {
                    return type == Material.FURNACE || type == Material.BURNING_FURNACE;
                }
            },
    NORMAL(Material.FURNACE),
    BURNING(Material.BURNING_FURNACE);

    private final Material type;

    FurnaceMaterials(Material type)
    {
        this.type = type;
    }

    public Material asBukkit()
    {
        return this.type;
    }

    public boolean equalsType(Material type)
    {
        return this.type == type;
    }

}
