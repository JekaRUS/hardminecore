package ru.jekarus.hardmine.api.structure.access;

import com.sun.istack.internal.NotNull;
import ru.jekarus.hardmine.api.structure.StructureDeserializer;
import ru.jekarus.hardmine.api.structure.StructureSerializer;
import ru.jekarus.hardmine.impl.structure.access.HmcStructureAccess;

import java.util.Collection;

public interface StructureAccess extends StructureSerializer, StructureDeserializer {

    void add(@NotNull StructureAccessUser user);

    void remove(@NotNull StructureAccessUser user);

    @NotNull
    Collection<StructureAccessUser> asCollection();


    static StructureAccess of()
    {
        return new HmcStructureAccess();
    }

    static StructureAccess of(Collection<StructureAccessUser> users)
    {
        return new HmcStructureAccess(users);
    }

}
