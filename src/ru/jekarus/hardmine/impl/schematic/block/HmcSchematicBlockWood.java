package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Tree;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockWood;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.schematic.block.utils.WoodFaces;
import ru.jekarus.hardmine.api.schematic.block.utils.WoodMaterials;

public class HmcSchematicBlockWood implements SchematicBlockWood {

    private WoodMaterials material;
    private WoodFaces face;

    public HmcSchematicBlockWood()
    {
        this(WoodMaterials.ANY, WoodFaces.ANY);
    }

    public HmcSchematicBlockWood(WoodMaterials material, WoodFaces face)
    {
        this.material = material;
        this.face = face;
    }

    @Override
    public WoodMaterials getMaterial()
    {
        return this.material;
    }

    @Override
    public void setMaterial(WoodMaterials material)
    {
        this.material = material;
    }

    @Override
    public WoodFaces getFace()
    {
        return this.face;
    }

    @Override
    public void setFace(WoodFaces face)
    {
        this.face = face;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return this.material.asBukkit();
    }

    @Override
    public void place(Block block)
    {
        block.setType(this.material.asBukkit());
        Tree wood = this.asBukkitWood(block);
        wood.setDirection(this.face.asBukkit());
        wood.setSpecies(this.material.asBukkitSpecies());
    }

    @Override
    public boolean compare(Block block)
    {
        if (!this.material.equalsType(block.getType()))
        {
            Bukkit.broadcastMessage(String.format("invalid type - %s - need - %s", block.getType(), this.material.asBukkit()));
            return false;
        }
        Tree wood = this.asBukkitWood(block);
        if (!this.material.equalsSpecies(wood.getSpecies()))
        {
            Bukkit.broadcastMessage(String.format("invalid mateiral - %s - need - %s", wood.getSpecies(), this.material.asBukkitSpecies()));
            return false;
        }
        if (!this.face.equalsFace(wood.getDirection()))
        {
            Bukkit.broadcastMessage(String.format("invalid face - %s - need - %s", wood.getDirection(), this.face.asBukkit()));
            return false;
        }
        return true;
    }

    private Tree asBukkitWood(Block block)
    {
        return (Tree) block.getState().getData();
    }

    @Override
    public SchematicBlockWood cloneBlock()
    {
        return new HmcSchematicBlockWood(this.material, this.face);
    }

    @Override
    public SchematicBlockWood cloneBlock(BlockRotate rotate)
    {
        if (this.face == WoodFaces.ANY)
        {
            return this.cloneBlock();
        }

        if (this.face == WoodFaces.UP)
        {
            return this.cloneBlock();
        }

        switch (rotate)
        {
            case R_90:
            case R_270:
                return new HmcSchematicBlockWood(this.material, this.face == WoodFaces.NORTH ? WoodFaces.WEST : WoodFaces.NORTH);
            default:
                return this.cloneBlock();
        }
    }

}