package ru.jekarus.hardmine;

import ru.jekarus.hardmine.api.HardMineApi;
import ru.jekarus.hardmine.api.StructureTypeContainer;
import ru.jekarus.hardmine.api.storage.StructureContainer;
import ru.jekarus.hardmine.impl.HmcStructureTypeContainer;
import ru.jekarus.hardmine.impl.storage.HmcStructureContainer;

public class HmcApi implements HardMineApi {

    private final HmcStructureContainer structureContainer;
    private final HmcStructureTypeContainer structureTypeContainer;

    public HmcApi()
    {
        this.structureContainer = new HmcStructureContainer();
        this.structureTypeContainer = new HmcStructureTypeContainer();
    }

    @Override
    public StructureContainer getStructureContainer()
    {
        return this.structureContainer;
    }

    @Override
    public StructureTypeContainer getStructureTypeContainer()
    {
        return this.structureTypeContainer;
    }

    public void disable()
    {
        this.structureContainer.disable();
    }

}
