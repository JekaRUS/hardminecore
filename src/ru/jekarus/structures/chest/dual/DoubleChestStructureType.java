package ru.jekarus.structures.chest.dual;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.schematic.RotatableSchematic;
import ru.jekarus.hardmine.api.schematic.RotatableSchematicBuilder;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.RotatableStructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public class DoubleChestStructureType implements RotatableStructureType {

    private final StructureTypeData data;
    private final StructureSize size;

    private final Collection<SchematicBlock> interactedBlocks;

    private final RotatableSchematic schematic;

    public DoubleChestStructureType(JavaPlugin plugin)
    {
        this.data = StructureTypeData.of(plugin, "double_chest");
        this.size = StructureSize.of(3, 4, 4);

        this.interactedBlocks = Collections.singletonList(SchematicBlock.chest());

        RotatableSchematicBuilder builder = RotatableSchematic.builder(this);
        builder
                .shape(0, "aaa", "aba", "aba", "aaa")
                .shape(1, "cbc", "bdb", "bdb", "cbc")
                .shape(2, "bbb", "beb", "beb", "bbb")
                .shape(3, "bbb", "beb", "beb", "bbb");
        builder
                .setBlock('a', SchematicBlock.of(Material.SMOOTH_BRICK))
                .setBlock('b', SchematicBlock.ban(SchematicBlock.chest()))
                .setBlock(
                        'c',
                        SchematicBlock.of(
                                SchematicBlock.of(Material.REDSTONE_TORCH_ON),
                                SchematicBlock.of(Material.REDSTONE_TORCH_OFF)
                        )
                )
                .setBlock('d', SchematicBlock.chest())
                .setBlock('e', SchematicBlock.ANY);
        this.schematic = (RotatableSchematic) builder.build();
    }

    @Override
    public Collection<BlockRotate> getRotates(SchematicBlock schematicBlock)
    {
        return Arrays.asList(BlockRotate.R_0, BlockRotate.R_90, BlockRotate.R_180, BlockRotate.R_270);
    }

    @Override
    public Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player, BlockRotate rotate)
    {
        Block minBlock = this.getMinBlock(block, schematicBlock, rotate);
        if (this.schematic.checkStructure(minBlock, rotate))
        {
            return Optional.of(new DoubleChestStructure(
                    this, StructureUser.of(player), StructureAccess.of(),
                    StructureData.of(
                            minBlock, this.size.getX(), this.size.getZ(), System.currentTimeMillis()
                    )
            ));
        }
        return Optional.empty();
    }

    private Block getMinBlock(Block block, SchematicBlock schematicBlock, BlockRotate rotate)
    {

        Block before = block;
        Block relative;
        for (int i = 1; i < 3; i++)
        {
            relative = block.getRelative(0, -i, 0);
            if (!schematicBlock.compare(relative))
            {
                break;
            }
            before = relative;
        }

        switch (rotate)
        {
            case R_0:
                return before.getRelative(-1, -1, -1);
            case R_180:
                return before.getRelative(-1, -1, -2);
            case R_90:
                return before.getRelative(-1, -1, -1);
            case R_270:
                return before.getRelative(-2, -1, -1);
        }
        return null;
    }

    @Override
    public StructureTypeData getData()
    {
        return this.data;
    }

    @Override
    public StructureSize getSize()
    {
        return this.size;
    }

    @Override
    public Structure createEmpty()
    {
        return new DoubleChestStructure(this);
    }

    @Override
    public Collection<SchematicBlock> getInteractedBlocks()
    {
        return this.interactedBlocks;
    }
}
