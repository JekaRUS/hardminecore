package ru.jekarus.hardmine.api;

import ru.jekarus.hardmine.api.storage.StructureContainer;

public interface HardMineApi {

    StructureContainer getStructureContainer();

    StructureTypeContainer getStructureTypeContainer();

}
