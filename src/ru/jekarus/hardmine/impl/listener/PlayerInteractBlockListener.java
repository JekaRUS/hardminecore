package ru.jekarus.hardmine.impl.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import ru.jekarus.hardmine.api.StructureTypeContainer;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.storage.StructureContainer;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.type.RotatableStructureType;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.HmcStructureTypeContainer;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

public class PlayerInteractBlockListener implements Listener {

    private final StructureContainer structureContainer;
    private final HmcStructureTypeContainer structureTypeContainer;

    public PlayerInteractBlockListener(StructureContainer structureContainer, StructureTypeContainer structureTypeContainer)
    {
        this.structureContainer = structureContainer;
        this.structureTypeContainer = (HmcStructureTypeContainer) structureTypeContainer;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {

        Block block = event.getClickedBlock();
        if (block == null || event.getAction() != Action.RIGHT_CLICK_BLOCK)
        {
            return;
        }
        Material type = block.getType();

        Map<Material, Collection<StructureType>> interactedBlocks = this.structureTypeContainer.asInteractedBlocks();

        Collection<StructureType> structureTypes = interactedBlocks.get(type);
        if (structureTypes == null)
        {
            return;
        }

        Optional<Structure> existsStructure = this.structureContainer.getStructure(block.getWorld(), block.getChunk().getX(), block.getChunk().getZ(), block.getX(), block.getY(), block.getZ());
        if (existsStructure.isPresent())
        {
            System.out.println("exists");
            return;
        }

        boolean deny = true;
        types:
        for (StructureType structureType : structureTypes)
        {
            for (SchematicBlock schematicBlock : structureType.getInteractedBlocks())
            {
                if (schematicBlock.compare(block))
                {
                    if (structureType.getMod() == StructureType.Mods.NORMAL)
                    {
                        Optional<Structure> structureOptional = structureType.tryCreate(block, schematicBlock, event.getPlayer());
                        if (structureOptional.isPresent())
                        {
                            this.structureContainer.register(structureOptional.get());
                            Bukkit.broadcastMessage("created");
                            deny = false;
                            break types;
                        }
                    }
                    else
                    {
                        RotatableStructureType rotatable = (RotatableStructureType) structureType;
                        Optional<Structure> structureOptional;
                        for (BlockRotate rotate : rotatable.getRotates(schematicBlock))
                        {
                            structureOptional = rotatable.tryCreate(block, schematicBlock, event.getPlayer(), rotate);
                            if (structureOptional.isPresent())
                            {
                                this.structureContainer.register(structureOptional.get());
                                Bukkit.broadcastMessage("created");
                                deny = false;
                                break types;
                            }
                        }
                    }
                }
            }
        }

        if (deny)
        {
            event.setUseInteractedBlock(Event.Result.DENY);
        }

    }

}