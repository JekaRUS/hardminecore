package ru.jekarus.hardmine.impl.listener;

import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.HardMineApi;

import java.util.Arrays;
import java.util.List;

public class HmcListeners {

    private final HardMineApi api;
    private List<Listener> listeners;

    public HmcListeners(HardMineApi api)
    {
        this.api = api;

        this.listeners = Arrays.asList(
                new PlayerInteractBlockListener(api.getStructureContainer(), api.getStructureTypeContainer())
        );
    }

    public void register(JavaPlugin plugin)
    {
        for (Listener listener : this.listeners)
        {
            plugin.getServer().getPluginManager().registerEvents(listener, plugin);
        }
    }

    public void unregister()
    {
        for (Listener listener : this.listeners)
        {
            HandlerList.unregisterAll(listener);
        }
    }

}
