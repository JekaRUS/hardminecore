package ru.jekarus.hardmine.api.structure.mark;

import com.sun.istack.internal.NotNull;

import java.util.Collection;
import java.util.Optional;

public interface MarksContainer {

    void registerMark(@NotNull MarkData mark);

    @NotNull
    Optional<MarkData> getMark(@NotNull String string);

    @NotNull
    Optional<MarkData> getMark(@NotNull int id);

    @NotNull
    Collection<MarkData> asCollection();

}
