package ru.jekarus.hardmine.api.structure;

import ru.jekarus.hardmine.impl.structure.HmcStructureSize;

public interface StructureSize {

    int getX();

    int getY();

    int getZ();

    static StructureSize of(int dx, int dy, int dz)
    {
        return new HmcStructureSize(dx, dy, dz);
    }
}
