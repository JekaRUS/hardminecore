package ru.jekarus.hardmine.api.structure.type;

public final class StructureTypeTitle {

    private static int ID = 0;

    private final String title;
    private final int ordinal;

    private StructureTypeTitle(String title)
    {
        this.title = title;
        this.ordinal = ID++;
    }

    public static StructureTypeTitle newTitle(String title)
    {

        if (title == null)
        {
            throw new NullPointerException();
        }

        return new StructureTypeTitle(title);

    }

    public final int ordinal()
    {
        return this.ordinal;
    }

    @Override
    public final String toString()
    {
        return this.title;
    }

    @Override
    public final boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        StructureTypeTitle that = (StructureTypeTitle) o;

        return ordinal == that.ordinal;
    }

    @Override
    public final int hashCode()
    {
        return ordinal;
    }

}
