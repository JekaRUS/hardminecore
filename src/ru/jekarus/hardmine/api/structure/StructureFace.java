package ru.jekarus.hardmine.api.structure;

import org.bukkit.block.BlockFace;

public enum StructureFace {

    NORTH(
            BlockFace.NORTH
    ),
    EAST(
            BlockFace.EAST
    ),
    SOUTH(
            BlockFace.SOUTH
    ),
    WEST(
            BlockFace.WEST
    );

    private final BlockFace face;

    StructureFace(BlockFace face)
    {
        this.face = face;
    }

    public BlockFace asBlockFace()
    {
        return this.face;
    }

}
