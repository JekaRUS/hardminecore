package ru.jekarus.hardmine.api.storage;

import ru.jekarus.hardmine.api.structure.Structure;

import java.util.Optional;

public interface StorageStructureWorld {

    void register(Structure structure);

    void remove(Structure structure);

    boolean contains(Structure structure);

    Optional<Structure> getStructure(int chunkX, int chunkZ, int x, int y, int z);

}
