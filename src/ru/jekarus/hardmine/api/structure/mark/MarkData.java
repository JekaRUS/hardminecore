package ru.jekarus.hardmine.api.structure.mark;

import com.sun.istack.internal.NotNull;
import ru.jekarus.hardmine.api.structure.StructureDeserializer;
import ru.jekarus.hardmine.api.structure.StructureSerializer;

public interface MarkData extends StructureSerializer, StructureDeserializer {

    @NotNull
    MarkId getId();

    @NotNull
    String getName();

    @NotNull
    MarkValue getValue();

    void setValue(@NotNull MarkValue value);

}
