package ru.jekarus.hardmine.api.schematic.block.utils;

import org.bukkit.Material;

public enum StairsMaterials {

    ANY(Material.WOOD_STAIRS)
            {
                @Override
                public boolean equalsType(Material type)
                {
                    for (StairsMaterials material : StairsMaterials.values())
                    {
                        if (material != ANY && type == material.type)
                        {
                            return true;
                        }
                    }
                    return false;
                }
            },
    OAK(Material.WOOD_STAIRS),
    COBBLESTONE(Material.COBBLESTONE_STAIRS),
    BRICK(Material.BRICK_STAIRS),
    SMOOTH(Material.SMOOTH_STAIRS),
    QUARTZ(Material.QUARTZ_STAIRS),
    NETHER(Material.NETHER_BRICK_STAIRS),
    SANDSTONE(Material.SANDSTONE_STAIRS),
    SPRUCE(Material.SPRUCE_WOOD_STAIRS),
    BIRCH(Material.BIRCH_WOOD_STAIRS),
    JUNGLE(Material.JUNGLE_WOOD_STAIRS),
    ACACIA(Material.ACACIA_STAIRS),
    DARK_OAK(Material.DARK_OAK_STAIRS),
    RED_SANDSTONE(Material.RED_SANDSTONE_STAIRS),
    PURPUR(Material.PURPUR_STAIRS);

    private final Material type;

    StairsMaterials(Material type)
    {
        this.type = type;
    }

    public Material asBukkit()
    {
        return this.type;
    }

    public boolean equalsType(Material type)
    {
        return this.type == type;
    }

}
