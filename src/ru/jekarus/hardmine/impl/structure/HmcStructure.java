package ru.jekarus.hardmine.impl.structure;

import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureUniqueId;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.structure.access.HmcStructureAccess;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class HmcStructure implements Structure {

    private static int save_version = 0;

    private StructureType type;

    private StructureUser owner;
    private StructureAccess access;
    private StructureData data;
    private StructureUniqueId uniqueId;

    public HmcStructure(
            StructureType type,
            StructureUser owner,
            StructureAccess access,
            StructureData data
    )
    {
        this.type = type;
        this.owner = owner;
        this.access = access;
        this.data = data;

        this.uniqueId = StructureUniqueId.newUniqueId(this);
    }

    public HmcStructure(StructureType type)
    {
        this.type = type;
    }

    @Override
    public StructureUser getOwner()
    {
        return this.owner;
    }

    @Override
    public StructureAccess getAccess()
    {
        return this.access;
    }

    @Override
    public StructureData getData()
    {
        return this.data;
    }

    @Override
    public StructureType getType()
    {
        return this.type;
    }

    @Override
    public StructureUniqueId getUniqueId()
    {
        return this.uniqueId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        HmcStructure that = (HmcStructure) o;

        return uniqueId.equals(that.uniqueId);
    }

    @Override
    public int hashCode()
    {
        return uniqueId.hashCode();
    }

    @Override
    public void read(DataInputStream stream) throws IOException
    {
        int save_version = stream.readInt();

        this.owner = new HmcStructureUser() {{
            this.read(stream);
        }};
        this.access = new HmcStructureAccess() {{
            this.read(stream);
        }};
        this.data = new HmcStructureData() {{
            this.read(stream);
        }};
    }

    @Override
    public void write(DataOutputStream stream) throws IOException
    {
        stream.writeInt(save_version);

        this.owner.write(stream);
        this.access.write(stream);
        this.data.write(stream);
    }
}
