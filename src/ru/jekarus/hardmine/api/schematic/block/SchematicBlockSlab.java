package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.BlockPositions;
import ru.jekarus.hardmine.api.schematic.block.utils.SlabMaterials;

public interface SchematicBlockSlab extends SchematicBlock<SchematicBlockSlab> {

    SlabMaterials getMaterial();

    void setMaterial(SlabMaterials material);

    BlockPositions getPosition();

    void setPosition(BlockPositions position);

}
