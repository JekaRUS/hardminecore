package ru.jekarus.hardmine.api.schematic;

import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.schematic.HmcSchematicBuilder;

public interface Schematic {

    static SchematicBuilder builder(StructureType type)
    {
        return new HmcSchematicBuilder(type);
    }

    boolean checkStructure(Block block);

    boolean checkChangedBlock(Block minBlock, Block changedBlock);

}
