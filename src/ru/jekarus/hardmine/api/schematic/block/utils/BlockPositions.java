package ru.jekarus.hardmine.api.schematic.block.utils;

public enum BlockPositions {

    ANY,
    TOP,
    BOTTOM

}
