package ru.jekarus.hardmine.impl.structure;

import ru.jekarus.hardmine.api.structure.StructureSize;

public class HmcStructureSize implements StructureSize {

    private final int dx;
    private final int dy;
    private final int dz;

    public HmcStructureSize(int dx, int dy, int dz)
    {
        this.dx = dx;
        this.dy = dy;
        this.dz = dz;
    }

    @Override
    public int getX()
    {
        return this.dx;
    }

    @Override
    public int getY()
    {
        return this.dy;
    }

    @Override
    public int getZ()
    {
        return this.dz;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        HmcStructureSize that = (HmcStructureSize) o;

        return dx == that.dx && dy == that.dy && dz == that.dz;
    }

    @Override
    public int hashCode()
    {
        int result = dx;
        result = 31 * result + dy;
        result = 31 * result + dz;
        return result;
    }
}
