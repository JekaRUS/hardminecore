package ru.jekarus.hardmine.impl.storage;

import ru.jekarus.hardmine.api.storage.StorageStructureChunk;
import ru.jekarus.hardmine.api.storage.StorageStructures;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureChunk;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;
import ru.jekarus.hardmine.api.structure.type.StructureTypeTitle;

import java.util.HashMap;
import java.util.Optional;

public class HmcStorageStructureChunk implements StorageStructureChunk {

    private StructureChunk chunk;
    private HashMap<StructureTypeTitle, StorageStructures> structures;

    public HmcStorageStructureChunk(StructureChunk chunk)
    {
        this.chunk = chunk;
        this.structures = new HashMap<>();
    }

    @Override
    public void register(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureType type = structure.getType();
        if (type == null)
        {
            throw new NullPointerException();
        }
        StructureTypeData data = type.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureTypeTitle title = data.getTitle();
        if (title == null)
        {
            throw new NullPointerException();
        }

        StorageStructures structures = this.structures.get(title);
        if (structures == null)
        {
            structures = StorageStructures.of(type);
            this.structures.put(title, structures);
        }
        structures.register(structure);
    }

    @Override
    public void remove(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureType type = structure.getType();
        if (type == null)
        {
            throw new NullPointerException();
        }
        StructureTypeData data = type.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureTypeTitle title = data.getTitle();
        if (title == null)
        {
            throw new NullPointerException();
        }

        StorageStructures structures = this.structures.get(title);
        if (structures != null)
        {
            structures.remove(structure);
        }
    }

    @Override
    public boolean contains(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureType type = structure.getType();
        if (type == null)
        {
            throw new NullPointerException();
        }
        StructureTypeData data = type.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureTypeTitle title = data.getTitle();
        if (title == null)
        {
            throw new NullPointerException();
        }

        StorageStructures structures = this.structures.get(title);
        return structures != null && structures.contains(structure);
    }

    @Override
    public Optional<Structure> getStructure(int x, int y, int z)
    {
        Optional<Structure> structure;
        for (StorageStructures structures : this.structures.values())
        {
            structure = structures.getStructure(x, y, z);
            if (structure.isPresent())
            {
                return structure;
            }
        }
        return Optional.empty();
    }
}
