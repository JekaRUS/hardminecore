package ru.jekarus.hardmine.impl.storage;

import org.bukkit.Bukkit;
import org.bukkit.World;
import ru.jekarus.hardmine.api.storage.StorageStructureChunk;
import ru.jekarus.hardmine.api.storage.StorageStructureWorld;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureChunk;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureLocation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

public class HmcStorageStructureWorld implements StorageStructureWorld {

    private World world;
    private HashMap<StructureChunk, StorageStructureChunk> chunks;

    public HmcStorageStructureWorld(World world)
    {
        this.world = world;
        this.chunks = new HashMap<>();
    }

    @Override
    public void register(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureData data = structure.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureLocation location = data.getLocation();
        if (location == null)
        {
            throw new NullPointerException();
        }
        Collection<StructureChunk> chunks = location.getChunks();
        if (chunks == null)
        {
            throw new NullPointerException();
        }

        StorageStructureChunk structureChunk = null;
        for (StructureChunk chunk : chunks)
        {
            structureChunk = this.chunks.get(chunk);
            if (structureChunk == null)
            {
                chunk = StructureChunk.of(chunk);
                structureChunk = StorageStructureChunk.of(chunk);
                this.chunks.put(chunk, structureChunk);
            }
            structureChunk.register(structure);
        }
    }

    @Override
    public void remove(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureData data = structure.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureLocation location = data.getLocation();
        if (location == null)
        {
            throw new NullPointerException();
        }
        Collection<StructureChunk> chunks = location.getChunks();
        if (chunks == null)
        {
            throw new NullPointerException();
        }

        StorageStructureChunk structureChunk = null;
        for (StructureChunk chunk : chunks)
        {
            structureChunk = this.chunks.get(chunk);
            if (structureChunk != null)
            {
                structureChunk.remove(structure);
            }
        }
    }

    @Override
    public boolean contains(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureData data = structure.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureLocation location = data.getLocation();
        if (location == null)
        {
            throw new NullPointerException();
        }
        Collection<StructureChunk> chunks = location.getChunks();
        if (chunks == null)
        {
            throw new NullPointerException();
        }

        StorageStructureChunk structureChunk = null;
        for (StructureChunk chunk : chunks)
        {
            structureChunk = this.chunks.get(chunk);
            if (structureChunk != null)
            {
                return structureChunk.contains(structure);
            }
        }
        return false;
    }

    @Override
    public Optional<Structure> getStructure(int chunkX, int chunkZ, int x, int y, int z)
    {
        StorageStructureChunk structureChunk = this.chunks.get(StructureChunk.of(chunkX, chunkZ));

        if (structureChunk == null)
        {
            return Optional.empty();
        }
        else
        {
            Bukkit.broadcastMessage("cont find world");
            return structureChunk.getStructure(x, y, z);
        }
    }

    @Override
    public final boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        HmcStorageStructureWorld that = (HmcStorageStructureWorld) o;

        return this.world.getUID().equals(that.world.getUID());
    }

    @Override
    public final int hashCode()
    {
        return this.world.getUID().hashCode();
    }

}
