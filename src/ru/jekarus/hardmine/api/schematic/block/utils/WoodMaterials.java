package ru.jekarus.hardmine.api.schematic.block.utils;

import org.bukkit.Material;
import org.bukkit.TreeSpecies;

public enum WoodMaterials {

    ANY(Material.LOG, TreeSpecies.GENERIC)
            {
                @Override
                public boolean equalsType(Material type)
                {
                    return type == Material.LOG || type == Material.LOG_2;
                }

                @Override
                public boolean equalsSpecies(TreeSpecies species)
                {
                    return true;
                }
            },
    OAK(Material.LOG, TreeSpecies.GENERIC),
    SPRUCE(Material.LOG, TreeSpecies.REDWOOD),
    BIRCH(Material.LOG, TreeSpecies.BIRCH),
    JUNGLE(Material.LOG, TreeSpecies.JUNGLE),
    ACACIA(Material.LOG_2, TreeSpecies.ACACIA),
    DARK_OAK(Material.LOG_2, TreeSpecies.DARK_OAK);

    private final Material type;
    private final TreeSpecies species;

    WoodMaterials(Material type, TreeSpecies species)
    {
        this.type = type;
        this.species = species;
    }

    public Material asBukkit()
    {
        return this.type;
    }

    public TreeSpecies asBukkitSpecies()
    {
        return this.species;
    }

    public boolean equalsType(Material type)
    {
        return this.type == type;
    }

    public boolean equalsSpecies(TreeSpecies species)
    {
        return this.species == species;
    }

}
