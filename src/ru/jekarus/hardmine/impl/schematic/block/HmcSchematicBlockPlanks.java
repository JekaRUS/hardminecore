package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Wood;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockPlanks;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.schematic.block.utils.PlanksMaterials;

public class HmcSchematicBlockPlanks implements SchematicBlockPlanks {

    private PlanksMaterials material;

    public HmcSchematicBlockPlanks()
    {
        this(PlanksMaterials.ANY);
    }

    public HmcSchematicBlockPlanks(PlanksMaterials material)
    {
        this.material = material;
    }

    @Override
    public PlanksMaterials getMaterial()
    {
        return this.material;
    }

    @Override
    public void setMaterial(PlanksMaterials material)
    {
        this.material = material;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return this.material.asBukkit();
    }

    @Override
    public void place(Block block)
    {
        block.setType(this.material.asBukkit());
        this.asBukkitWood(block).setSpecies(this.material.asBukkitSpecies());
    }

    @Override
    public boolean compare(Block block)
    {
        if (!this.material.equalsType(block.getType()))
        {
            return false;
        }
        Wood wood = this.asBukkitWood(block);
        if (!this.material.equalsSpecies(wood.getSpecies()))
        {
            return false;
        }
        return true;
    }

    private Wood asBukkitWood(Block block)
    {
        return (Wood) block.getState().getData();
    }

    @Override
    public SchematicBlockPlanks cloneBlock()
    {
        return new HmcSchematicBlockPlanks(this.material);
    }

    @Override
    public SchematicBlockPlanks cloneBlock(BlockRotate rotate)
    {
        return this.cloneBlock();
    }

}
