package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Bed;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockBed;
import ru.jekarus.hardmine.api.schematic.block.utils.BedParts;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockColors;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockFaces;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;

public class HmcSchematicBlockBed extends HmcSchematicBlockFaced<SchematicBlockBed> implements SchematicBlockBed {

    protected BedParts part;
    protected BlockColors color;

    public HmcSchematicBlockBed()
    {
        this(BedParts.ANY, BlockColors.ANY, BlockFaces.ANY);
    }

    public HmcSchematicBlockBed(BedParts part, BlockColors color, BlockFaces face)
    {
        super(face);
        this.part = part;
        this.color = color;
    }

    @Override
    public BedParts getPart()
    {
        return this.part;
    }

    @Override
    public void setPart(BedParts part)
    {
        this.part = part;
    }

    @Override
    public BlockColors getColor()
    {
        return this.color;
    }

    @Override
    public void setColor(BlockColors color)
    {
        this.color = color;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return Material.BED_BLOCK;
    }

    @Override
    public void place(Block block)
    {
        block.setType(Material.BED_BLOCK);
        org.bukkit.block.Bed bedState = this.asBukkitBedState(block);
        bedState.setColor(this.color.asBukkit());
        Bed bedMaterial = this.asBukkitBedMaterial(block);
        bedMaterial.setHeadOfBed(this.part != BedParts.BOTTOM);
        bedMaterial.setFacingDirection(this.face.asBukkit());
    }

    @Override
    public boolean compare(Block block)
    {
        if (block.getType() != this.asBukkitMaterial())
        {
            return false;
        }
        org.bukkit.block.Bed bedState = this.asBukkitBedState(block);
        if (!this.color.equalsColor(bedState.getColor()))
        {
            Bukkit.broadcastMessage(String.format("invalid color - %s - need - %s", bedState.getColor(), this.color.asBukkit()));
            return false;
        }
        org.bukkit.material.Bed bedMaterial = this.asBukkitBedMaterial(block);
        if (this.part != BedParts.ANY)
        {
            if (this.part == BedParts.TOP && !bedMaterial.isHeadOfBed() || this.part == BedParts.BOTTOM && bedMaterial.isHeadOfBed())
            {
                Bukkit.broadcastMessage(String.format("invalid part - %s - need - %s", bedMaterial.isHeadOfBed(), this.part == BedParts.TOP));
                return false;
            }
        }
        if (!this.face.equalsFace(bedMaterial.getFacing()))
        {
            Bukkit.broadcastMessage(String.format("invalid face - %s - need - %s", bedMaterial.getFacing(), this.face.asBukkit()));
            return false;
        }
        return true;
    }

    protected org.bukkit.material.Bed asBukkitBedMaterial(Block block)
    {
        return (org.bukkit.material.Bed) block.getState().getData();
    }

    protected org.bukkit.block.Bed asBukkitBedState(Block block)
    {
        return (org.bukkit.block.Bed) block.getState();
    }

    @Override
    public SchematicBlockBed cloneBlock()
    {
        return new HmcSchematicBlockBed(this.part, this.color, this.face);
    }

    @Override
    public SchematicBlockBed cloneBlock(BlockRotate rotate)
    {
        return new HmcSchematicBlockBed(this.part, this.color, BlockFaces.rotate(this.face, rotate));
    }
}
