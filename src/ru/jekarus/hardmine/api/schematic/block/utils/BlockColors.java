package ru.jekarus.hardmine.api.schematic.block.utils;

import org.bukkit.DyeColor;

public enum BlockColors {

    ANY(DyeColor.WHITE)
            {
                @Override
                public boolean equalsColor(DyeColor color)
                {
                    return true;
                }
            },
    WHITE(DyeColor.WHITE),
    ORANGE(DyeColor.ORANGE),
    MAGENTA(DyeColor.MAGENTA),
    LIGHT_BLUE(DyeColor.LIGHT_BLUE),
    YELLOW(DyeColor.YELLOW),
    LIME(DyeColor.LIME),
    PINK(DyeColor.PINK),
    GRAY(DyeColor.GRAY),
    SILVER(DyeColor.SILVER),
    CYAN(DyeColor.CYAN),
    PURPLE(DyeColor.PURPLE),
    BLUE(DyeColor.BLUE),
    BROWN(DyeColor.BROWN),
    GREEN(DyeColor.GREEN),
    RED(DyeColor.RED),
    BLACK(DyeColor.BLACK);

    private final DyeColor bukkitColor;

    BlockColors(DyeColor bukkitColor)
    {
        this.bukkitColor = bukkitColor;
    }

    public DyeColor asBukkit()
    {
        return this.bukkitColor;
    }

    public boolean equalsColor(DyeColor color)
    {
        return this.bukkitColor == color;
    }

}
