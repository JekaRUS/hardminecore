package ru.jekarus.structures.furnace.advanced;

import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.structure.HmcStructure;

public class AdvancedFurnaceStructure extends HmcStructure {

    public AdvancedFurnaceStructure(StructureType type, StructureUser owner, StructureAccess access, StructureData data)
    {
        super(type, owner, access, data);
    }

    public AdvancedFurnaceStructure(StructureType type)
    {
        super(type);
    }

}
