package ru.jekarus.structures.furnace.basic;

import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.structure.HmcStructure;

public class BasicFurnaceStructure extends HmcStructure {

    public BasicFurnaceStructure(StructureType type, StructureUser owner, StructureAccess access, StructureData data)
    {
        super(type, owner, access, data);
    }

    public BasicFurnaceStructure(StructureType type)
    {
        super(type);
    }

}
