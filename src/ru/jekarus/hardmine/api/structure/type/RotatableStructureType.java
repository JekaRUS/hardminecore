package ru.jekarus.hardmine.api.structure.type;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.structure.Structure;

import java.util.Collection;
import java.util.Optional;

public interface RotatableStructureType extends StructureType {

    Collection<BlockRotate> getRotates(SchematicBlock schematicBlock);

    Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player, BlockRotate rotate);

    @Override
    default Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player)
    {
        return this.tryCreate(block, schematicBlock, player, BlockRotate.R_0);
    }

    @Override
    default Mods getMod()
    {
        return Mods.ROTATABLE;
    }
}
