package ru.jekarus.hardmine.impl.storage;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.storage.StorageStructureWorld;
import ru.jekarus.hardmine.api.storage.StructureContainer;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureLocation;

import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

public class HmcStructureContainer implements StructureContainer {

    private HashMap<UUID, StorageStructureWorld> worlds;
    private Server server;

    public HmcStructureContainer()
    {
        this.worlds = new HashMap<>();
        this.server = Bukkit.getServer();
    }

    @Override
    public void register(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureData data = structure.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureLocation location = data.getLocation();
        if (location == null)
        {
            throw new NullPointerException();
        }
        World world = location.getWorld();
        if (world == null)
        {
            throw new NullPointerException();
        }

        StorageStructureWorld structureWorld = this.worlds.get(world.getUID());
        if (structureWorld == null)
        {
            structureWorld = new HmcStorageStructureWorld(world);
            this.worlds.put(world.getUID(), structureWorld);
        }

        structureWorld.register(structure);
    }

    @Override
    public void remove(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureData data = structure.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureLocation location = data.getLocation();
        if (location == null)
        {
            throw new NullPointerException();
        }
        World world = location.getWorld();
        if (world == null)
        {
            throw new NullPointerException();
        }

        StorageStructureWorld structureWorld = this.worlds.get(world.getUID());
        if (structureWorld != null)
        {
            structureWorld.remove(structure);
        }
    }

    @Override
    public boolean contains(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }
        StructureData data = structure.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }
        StructureLocation location = data.getLocation();
        if (location == null)
        {
            throw new NullPointerException();
        }
        World world = location.getWorld();
        if (world == null)
        {
            throw new NullPointerException();
        }

        StorageStructureWorld structureWorld = this.worlds.get(world.getUID());
        if (structureWorld != null)
        {
            return structureWorld.contains(structure);
        }
        return false;
    }

    @Override
    public Optional<Structure> getStructure(int x, int y, int z)
    {
        return this.getStructure(x / 16, z / 16, x, y, z);
    }

    @Override
    public Optional<Structure> getStructure(int chunkX, int chunkZ, int x, int y, int z)
    {
        return this.getStructure(this.server.getWorlds().get(0), chunkX, chunkZ, x, y, z);
    }

    @Override
    public Optional<Structure> getStructure(Block block)
    {
        if (block == null)
        {
            throw new NullPointerException();
        }
        return this.getStructure(block.getWorld(), block.getChunk().getX(), block.getChunk().getZ(), block.getX(), block.getY(), block.getZ());
    }

    @Override
    public Optional<Structure> getStructure(World world, int chunkX, int chunkZ, int x, int y, int z)
    {
        if (world == null)
        {
            throw new NullPointerException();
        }

        StorageStructureWorld structureWorld = this.worlds.get(world.getUID());

        if (structureWorld == null)
        {
            return Optional.empty();
        }
        else
        {
            Bukkit.broadcastMessage("cont find structureContainer");
            return structureWorld.getStructure(chunkX, chunkZ, x, y, z);
        }
    }

    public void disable()
    {

    }
}
