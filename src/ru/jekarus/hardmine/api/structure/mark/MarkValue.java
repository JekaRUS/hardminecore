package ru.jekarus.hardmine.api.structure.mark;

import com.sun.istack.internal.NotNull;
import ru.jekarus.hardmine.api.structure.StructureDeserializer;
import ru.jekarus.hardmine.api.structure.StructureSerializer;

public interface MarkValue extends StructureSerializer, StructureDeserializer {

    @NotNull
    String asString();

}
