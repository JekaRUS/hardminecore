package ru.jekarus.structures.crafting.table.basic;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.HardMineApi;
import ru.jekarus.hardmine.api.schematic.Schematic;
import ru.jekarus.hardmine.api.schematic.SchematicBuilder;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.storage.StructureContainer;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class BasicCraftingTableStructureType implements StructureType {

    private final HardMineApi api;

    private final StructureTypeData data;
    private final StructureSize size;

    private final List<SchematicBlock> interactedBlocks;

    private final Schematic schematic;

    public BasicCraftingTableStructureType(JavaPlugin plugin, HardMineApi api, BasicCraftingTableTypes type)
    {
        this.api = api;
        this.data = StructureTypeData.of(plugin, "basic_crafting_table_" + type.getName());
        this.size = StructureSize.of(3, 2, 3);

        this.interactedBlocks = Collections.singletonList(SchematicBlock.of(Material.WORKBENCH));

        SchematicBuilder builder = Schematic.builder(this);
        builder
                .shape(0, "aba", "bab", "aba")
                .shape(1, "cdc", "efe", "cdc");
        builder
                .setBlock('a', type.getTop())
                .setBlock('b', type.getPlanks())
                .setBlock('c', SchematicBlock.of(Material.WORKBENCH))
                .setBlock('d', type.getWest())
                .setBlock('e', type.getNorth())
                .setBlock('f', SchematicBlock.of(Material.GRAVEL));
        this.schematic = builder.build();
    }

    @Override
    public StructureTypeData getData()
    {
        return this.data;
    }

    @Override
    public StructureSize getSize()
    {
        return this.size;
    }

    @Override
    public Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player)
    {
        boolean create = false;
        Block minBlock = null;

        minBlock = block.getRelative(0, -1, 0);
        if (this.check(minBlock) && !this.checkExists(minBlock))
        {
            create = true;
        }

        if (!create)
        {
            minBlock = block.getRelative(-2, -1, 0);
            if (this.check(minBlock) && !this.checkExists(minBlock))
            {
                create = true;
            }
        }

        if (!create)
        {
            minBlock = block.getRelative(-2, -1, -2);
            if (this.check(minBlock) && !this.checkExists(minBlock))
            {
                create = true;
            }
        }

        if (!create)
        {
            minBlock = block.getRelative(0, -1, -2);
            if (this.check(minBlock) && !this.checkExists(minBlock))
            {
                create = true;
            }
        }

        if (create)
        {
            return Optional.of(new BasicCraftingTableStructure(
                    this, StructureUser.of(player), StructureAccess.of(),
                    StructureData.of(
                            minBlock, this.size.getX(), this.size.getZ(), System.currentTimeMillis()
                    )
            ));
        }

        return Optional.empty();
    }

    private boolean check(Block block)
    {
        return this.schematic.checkStructure(block);
    }

    private boolean checkExists(Block block)
    {
        StructureContainer container = this.api.getStructureContainer();
        Optional<Structure> optStructure;
        for (int x = 0; x < this.size.getX(); x++)
        {
            if (x == 1)
            {
                continue;
            }
            for (int y = 0; y < this.size.getY(); y++)
            {
                for (int z = 0; z < this.size.getZ(); z++)
                {
                    if (z == 1)
                    {
                        continue;
                    }
                    optStructure = container.getStructure(block.getRelative(x, y, z));
                    if (optStructure.isPresent())
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Structure createEmpty()
    {
        return new BasicCraftingTableStructure(this);
    }

    @Override
    public Collection<SchematicBlock> getInteractedBlocks()
    {
        return this.interactedBlocks;
    }

}
