package ru.jekarus.hardmine.api.schematic;

import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;

public interface SchematicBuilder {

    SchematicBuilder shape(int dy, String... shape);

    SchematicBuilder setBlock(char symbol, SchematicBlock block);

    Schematic build();

}
