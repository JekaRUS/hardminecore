package ru.jekarus.structures.enchanting.table;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.schematic.Schematic;
import ru.jekarus.hardmine.api.schematic.SchematicBuilder;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class EnchantingTableStructureType implements StructureType {

    private final StructureTypeData data;
    private final StructureSize size;

    private final List<SchematicBlock> interactedBlocks;

    private final Schematic schematic;

    public EnchantingTableStructureType(JavaPlugin plugin)
    {
        this.data = StructureTypeData.of(plugin, "enchanting_table");
        this.size = StructureSize.of(3, 2, 3);

        this.interactedBlocks = Collections.singletonList(SchematicBlock.of(Material.ENCHANTMENT_TABLE));

        SchematicBuilder builder = Schematic.builder(this);
        builder
                .shape(0, "pgp", "gdg", "pgp")
                .shape(1, "   ", " e ", "   ");
        builder
                .setBlock('p', SchematicBlock.of(Material.PRISMARINE))
                .setBlock('g', SchematicBlock.of(Material.GLOWSTONE))
                .setBlock('d', SchematicBlock.of(Material.DIAMOND_BLOCK))
                .setBlock('e', SchematicBlock.of(Material.ENCHANTMENT_TABLE))
                .setBlock(' ', SchematicBlock.ANY);
        this.schematic = builder.build();

    }

    @Override
    public StructureTypeData getData()
    {
        return this.data;
    }

    @Override
    public StructureSize getSize()
    {
        return this.size;
    }

    @Override
    public Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player)
    {
        Block relative = block.getRelative(-1, -1, -1);
        if (this.schematic.checkStructure(relative))
        {
            return Optional.of(new EnchantingTableStructure(
                    this, StructureUser.of(player), StructureAccess.of(),
                    StructureData.of(
                            relative, this.size.getX(), this.size.getZ(), System.currentTimeMillis()
                    )
            ));
        }
        return Optional.empty();
    }

    @Override
    public Structure createEmpty()
    {
        return new EnchantingTableStructure(this);
    }

    @Override
    public Collection<SchematicBlock> getInteractedBlocks()
    {
        return this.interactedBlocks;
    }
}
