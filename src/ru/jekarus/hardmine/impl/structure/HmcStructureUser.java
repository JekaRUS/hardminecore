package ru.jekarus.hardmine.impl.structure;

import ru.jekarus.hardmine.api.structure.StructureUser;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

public class HmcStructureUser implements StructureUser {

    private static int save_version = 0;

    private String name;
    private UUID uniqueId;

    public HmcStructureUser(
            String name,
            UUID uniqueId
    )
    {
        this.name = name;
        this.uniqueId = uniqueId;
    }

    public HmcStructureUser()
    {

    }

    @Override
    public Optional<String> getName()
    {
        return Optional.ofNullable(this.name);
    }

    @Override
    public Optional<UUID> getUniqueId()
    {
        return Optional.ofNullable(this.uniqueId);
    }

    @Override
    public void read(DataInputStream stream) throws IOException
    {
        int save_version = stream.readInt();

        this.name = stream.readUTF();
        this.uniqueId = new UUID(
                stream.readLong(),
                stream.readLong()
        );
    }

    @Override
    public void write(DataOutputStream stream) throws IOException
    {
        stream.writeInt(save_version);

        stream.writeUTF(this.name);
        stream.writeLong(this.uniqueId.getMostSignificantBits());
        stream.writeLong(this.uniqueId.getMostSignificantBits());
    }

}
