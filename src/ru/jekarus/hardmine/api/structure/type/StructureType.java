package ru.jekarus.hardmine.api.structure.type;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureSize;

import java.util.Collection;
import java.util.Optional;

public interface StructureType {

    StructureTypeData getData();

    StructureSize getSize();

    Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player);

    Structure createEmpty();

    Collection<SchematicBlock> getInteractedBlocks();

    default Mods getMod()
    {
        return Mods.NORMAL;
    }

    enum Mods {

        NORMAL,
        ROTATABLE

    }

}
