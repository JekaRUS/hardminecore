package ru.jekarus.hardmine.impl.schematic;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.SchematicLayout;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.structure.type.StructureType;

public class HmcSchematicLayout implements SchematicLayout {

    private final StructureType type;
    private final SchematicBlock[][] layout;

    public HmcSchematicLayout(SchematicBlock[][] layout)
    {
        this(null, layout);
    }

    public HmcSchematicLayout(StructureType type, SchematicBlock[][] layout)
    {
        this.type = type;
        this.layout = layout;
    }

    @Override
    public boolean checkStructure(Block block)
    {
        SchematicBlock[] schemZ;
        for (int x = 0; x < this.layout.length; x++)
        {
            schemZ = this.layout[x];
            for (int z = 0; z < schemZ.length; z++)
            {
                if (!schemZ[z].compare(block.getRelative(x, 0, z)))
                {
                    Block relative = block.getRelative(x, 0, z);
                    Bukkit.broadcastMessage(String.format("%d %d %s(%d %d %d) - %s", x, z, relative.getType(), relative.getX(), relative.getY(), relative.getZ(), schemZ[z].asBukkitMaterial()));
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean checkChangedBlock(Block minBlock, Block changedBlock)
    {
        return false; // FIXME: 03.01.2018
    }

}
