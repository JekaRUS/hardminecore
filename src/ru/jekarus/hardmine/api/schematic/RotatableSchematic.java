package ru.jekarus.hardmine.api.schematic;

import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.schematic.HmcRotatableSchematicBuilder;

public interface RotatableSchematic extends Schematic {

    static RotatableSchematicBuilder builder(StructureType type)
    {
        return new HmcRotatableSchematicBuilder(type);
    }

    boolean checkStructure(Block block, BlockRotate rotate);

    @Override
    default boolean checkStructure(Block block)
    {
        return this.checkStructure(block, BlockRotate.R_0);
    }

    boolean checkChangedBlock(Block minBlock, Block changedBlock, BlockRotate rotate);

    @Override
    default boolean checkChangedBlock(Block minBlock, Block changedBlock)
    {
        return this.checkChangedBlock(minBlock, changedBlock, BlockRotate.R_0);
    }

}
