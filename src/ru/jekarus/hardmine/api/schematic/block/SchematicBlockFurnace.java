package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.FurnaceMaterials;

public interface SchematicBlockFurnace extends SchematicBlockFaced<SchematicBlockFurnace> {

    FurnaceMaterials getMaterial();

    void setMaterial(FurnaceMaterials material);

}
