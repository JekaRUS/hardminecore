package ru.jekarus.structures.chest.basic;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.schematic.Schematic;
import ru.jekarus.hardmine.api.schematic.SchematicBuilder;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class BasicChestStructureType implements StructureType {

    private final StructureTypeData data;
    private final StructureSize size;

    private final List<SchematicBlock> interactedBlocks;

    private final Schematic schematic;

    public BasicChestStructureType(JavaPlugin plugin, BasicChestTypes type)
    {
        this.data = StructureTypeData.of(plugin, "basic_chest_" + type.getName());
        this.size = StructureSize.of(3, 4, 3);

        this.interactedBlocks = Collections.singletonList(SchematicBlock.chest());

        SchematicBuilder builder = Schematic.builder(this);
        builder
                .shape(0, "aba", "cdc", "aba")
                .shape(1, "ede", "dfd", "ede")
                .shape(2, "ddd", "dgd", "ddd")
                .shape(3, "ddd", "dgd", "ddd");
        builder
                .setBlock('a', type.getTop())
                .setBlock('b', type.getNorth())
                .setBlock('c', type.getWest())
                .setBlock('d', SchematicBlock.ban(SchematicBlock.chest()))
                .setBlock('e', SchematicBlock.of(Material.TORCH))
                .setBlock('f', SchematicBlock.chest())
                .setBlock('g', SchematicBlock.ANY);
        this.schematic = builder.build();

    }

    @Override
    public StructureTypeData getData()
    {
        return this.data;
    }

    @Override
    public StructureSize getSize()
    {
        return this.size;
    }

    @Override
    public Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player)
    {
        Block relative = this.getMinBlock(block, schematicBlock).getRelative(-1, -1, -1);
        if (this.schematic.checkStructure(relative))
        {
            return Optional.of(new BasicChestStructure(
                    this, StructureUser.of(player), StructureAccess.of(),
                    StructureData.of(
                            relative, this.size.getX(), this.size.getZ(), System.currentTimeMillis()
                    )
            ));
        }
        return Optional.empty();
    }

    private Block getMinBlock(Block block, SchematicBlock schematicBlock)
    {
        Block before = block;
        Block relative;
        for (int i = 1; i < 3; i++)
        {
            relative = block.getRelative(0, -i, 0);
            if (!schematicBlock.compare(relative))
            {
                return before;
            }
            before = relative;
        }
        return before;
    }

    @Override
    public Structure createEmpty()
    {
        return new BasicChestStructure(this);
    }

    @Override
    public Collection<SchematicBlock> getInteractedBlocks()
    {
        return this.interactedBlocks;
    }
}