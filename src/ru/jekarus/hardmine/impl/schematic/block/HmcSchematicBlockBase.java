package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockBase;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;

public class HmcSchematicBlockBase implements SchematicBlockBase {

    private Material material;

    public HmcSchematicBlockBase()
    {
        this(Material.STONE);
    }

    public HmcSchematicBlockBase(Material material)
    {
        this.material = material;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return this.material;
    }

    @Override
    public void place(Block block)
    {
        block.setType(this.material);
    }

    @Override
    public boolean compare(Block block)
    {
        return block.getType() == this.material;
    }

    @Override
    public SchematicBlockBase cloneBlock()
    {
        return new HmcSchematicBlockBase(this.material);
    }

    @Override
    public SchematicBlockBase cloneBlock(BlockRotate rotate)
    {
        return this.cloneBlock();
    }

}
