package ru.jekarus.structures.chest.ender;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.schematic.Schematic;
import ru.jekarus.hardmine.api.schematic.SchematicBuilder;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class EnderChestStructureType implements StructureType {

    private final StructureTypeData data;
    private final StructureSize size;

    private final List<SchematicBlock> interactedBlocks;

    private final Schematic schematic;

    public EnderChestStructureType(JavaPlugin plugin)
    {
        this.data = StructureTypeData.of(plugin, "ender_chest");
        this.size = StructureSize.of(3, 2, 3);

        this.interactedBlocks = Collections.singletonList(SchematicBlock.of(Material.ENDER_CHEST));

        SchematicBuilder builder = Schematic.builder(this);
        builder
                .shape(0, "aba", "bcb", "aba")
                .shape(1, "ddd", "ded", "ddd");
        builder
                .setBlock('a', SchematicBlock.of(Material.END_BRICKS))
                .setBlock('b', SchematicBlock.of(Material.PURPUR_BLOCK))
                .setBlock('c', SchematicBlock.of(Material.IRON_BLOCK))
                .setBlock('d', SchematicBlock.ban(SchematicBlock.of(Material.ENDER_CHEST)))
                .setBlock('e', SchematicBlock.of(Material.ENDER_CHEST));
        this.schematic = builder.build();

    }

    @Override
    public StructureTypeData getData()
    {
        return this.data;
    }

    @Override
    public StructureSize getSize()
    {
        return this.size;
    }

    @Override
    public Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player)
    {
        Block relative = block.getRelative(-1, -1, -1);
        if (this.schematic.checkStructure(relative))
        {
            return Optional.of(new EnderChestStructure(
                    this, StructureUser.of(player), StructureAccess.of(),
                    StructureData.of(
                            relative, this.size.getX(), this.size.getZ(), System.currentTimeMillis()
                    )
            ));
        }
        return Optional.empty();
    }

    @Override
    public Structure createEmpty()
    {
        return new EnderChestStructure(this);
    }

    @Override
    public Collection<SchematicBlock> getInteractedBlocks()
    {
        return this.interactedBlocks;
    }
}