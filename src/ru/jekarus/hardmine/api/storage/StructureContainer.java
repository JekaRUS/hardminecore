package ru.jekarus.hardmine.api.storage;

import org.bukkit.World;
import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.structure.Structure;

import java.util.Optional;

public interface StructureContainer {

    void register(Structure structure);

    void remove(Structure structure);

    boolean contains(Structure structure);

    Optional<Structure> getStructure(int x, int y, int z);

    Optional<Structure> getStructure(int chunkX, int chunkZ, int x, int y, int z);

    Optional<Structure> getStructure(Block block);

    Optional<Structure> getStructure(World world, int chunkX, int chunkZ, int x, int y, int z);

}
