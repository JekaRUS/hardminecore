package ru.jekarus.structures.furnace.auto;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.jekarus.hardmine.api.schematic.RotatableSchematic;
import ru.jekarus.hardmine.api.schematic.RotatableSchematicBuilder;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockFurnace;
import ru.jekarus.hardmine.api.schematic.block.utils.*;
import ru.jekarus.hardmine.api.structure.Structure;
import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureSize;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.RotatableStructureType;
import ru.jekarus.hardmine.api.structure.type.StructureTypeData;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public class AutoFurnaceStructureType implements RotatableStructureType {

    private final StructureTypeData data;
    private final StructureSize size;

    private final Collection<SchematicBlock> interactedBlocks;

    private final RotatableSchematic schematic;

    public AutoFurnaceStructureType(JavaPlugin plugin)
    {
        this.data = StructureTypeData.of(plugin, "auto_furnace");
        this.size = StructureSize.of(3, 3, 3);

        this.interactedBlocks = Arrays.asList(
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.EAST),
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.SOUTH),
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.WEST),
                SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.NORTH)
        );

        RotatableSchematicBuilder builder = RotatableSchematic.builder(this);
        builder
                .shape(0, "aab", "aca", "aab")
                .shape(1, "aad", "cef", "aag")
                .shape(2, "hij", "kbi", "hij");
        builder
                .setBlock('a', SchematicBlock.of(Material.SMOOTH_BRICK))
                .setBlock('b', SchematicBlock.chest(ChestMaterials.ANY, BlockFaces.ANY))
                .setBlock('c', SchematicBlock.of(Material.IRON_BLOCK))
                .setBlock('d', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.NORTH, BlockPositions.TOP))
                .setBlock('e', SchematicBlock.of(Material.REDSTONE_BLOCK))
                .setBlock('f', SchematicBlock.furnace(FurnaceMaterials.ANY, BlockFaces.EAST))
                .setBlock('g', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.SOUTH, BlockPositions.TOP))
                .setBlock('h', SchematicBlock.stairs(StairsMaterials.SMOOTH, BlockFaces.WEST, BlockPositions.BOTTOM))
                .setBlock('i', SchematicBlock.slab(SlabMaterials.STONE, BlockPositions.BOTTOM))
                .setBlock('j', SchematicBlock.ANY)
                .setBlock('k', SchematicBlock.doubleSlab(SlabMaterials.STONE));
        this.schematic = (RotatableSchematic) builder.build();
    }

    @Override
    public Collection<BlockRotate> getRotates(SchematicBlock schematicBlock)
    {
        SchematicBlockFurnace furnace = (SchematicBlockFurnace) schematicBlock;
        switch (furnace.getFace())
        {
            case EAST:
                return Collections.singletonList(BlockRotate.R_0);
            case SOUTH:
                return Collections.singletonList(BlockRotate.R_90);
            case WEST:
                return Collections.singletonList(BlockRotate.R_180);
            case NORTH:
                return Collections.singletonList(BlockRotate.R_270);
            default:
                return Arrays.asList(BlockRotate.R_0, BlockRotate.R_90, BlockRotate.R_180, BlockRotate.R_270);
        }
    }

    @Override
    public Optional<Structure> tryCreate(Block block, SchematicBlock schematicBlock, Player player, BlockRotate rotate)
    {
        Block minBlock = this.getMinBlock(block, rotate);
        if (this.schematic.checkStructure(minBlock, rotate))
        {
            return Optional.of(new AutoFurnaceStructure(
                    this, StructureUser.of(player), StructureAccess.of(),
                    StructureData.of(
                            minBlock, this.size.getX(), this.size.getZ(), System.currentTimeMillis()
                    )
            ));
        }
        return Optional.empty();
    }

    private Block getMinBlock(Block block, BlockRotate rotate)
    {
        switch (rotate)
        {
            case R_90:
                return block.getRelative(-1, -1, -2);
            case R_180:
                return block.getRelative(0, -1, -1);
            case R_270:
                return block.getRelative(-1, -1, 0);
            default:
                return block.getRelative(-2, -1, -1);
        }
    }

    @Override
    public StructureTypeData getData()
    {
        return this.data;
    }

    @Override
    public StructureSize getSize()
    {
        return this.size;
    }

    @Override
    public Structure createEmpty()
    {
        return new AutoFurnaceStructure(this);
    }

    @Override
    public Collection<SchematicBlock> getInteractedBlocks()
    {
        return this.interactedBlocks;
    }
}
