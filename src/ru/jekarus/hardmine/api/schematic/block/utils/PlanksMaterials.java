package ru.jekarus.hardmine.api.schematic.block.utils;

import org.bukkit.Material;
import org.bukkit.TreeSpecies;

public enum PlanksMaterials {

    ANY(Material.WOOD, TreeSpecies.GENERIC)
            {
                @Override
                public boolean equalsType(Material type)
                {
                    return type == Material.WOOD;
                }

                @Override
                public boolean equalsSpecies(TreeSpecies species)
                {
                    return true;
                }
            },
    OAK(Material.WOOD, TreeSpecies.GENERIC),
    SPRUCE(Material.WOOD, TreeSpecies.REDWOOD),
    BIRCH(Material.WOOD, TreeSpecies.BIRCH),
    JUNGLE(Material.WOOD, TreeSpecies.JUNGLE),
    ACACIA(Material.WOOD, TreeSpecies.ACACIA),
    DARK_OAK(Material.WOOD, TreeSpecies.DARK_OAK);

    private final Material type;
    private final TreeSpecies species;

    PlanksMaterials(Material type, TreeSpecies species)
    {
        this.type = type;
        this.species = species;
    }

    public Material asBukkit()
    {
        return this.type;
    }

    public TreeSpecies asBukkitSpecies()
    {
        return this.species;
    }

    public boolean equalsType(Material type)
    {
        return this.type == type;
    }

    public boolean equalsSpecies(TreeSpecies species)
    {
        return this.species == species;
    }

}
