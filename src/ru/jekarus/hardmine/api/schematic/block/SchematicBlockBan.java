package ru.jekarus.hardmine.api.schematic.block;

public interface SchematicBlockBan extends SchematicBlock<SchematicBlockBan> {

    SchematicBlock getBanned();

    void setBanned(SchematicBlock block);

}
