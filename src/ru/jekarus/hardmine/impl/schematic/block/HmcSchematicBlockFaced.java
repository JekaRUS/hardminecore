package ru.jekarus.hardmine.impl.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockFaced;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockFaces;

public abstract class HmcSchematicBlockFaced<B extends SchematicBlock> implements SchematicBlockFaced<B> {

    protected BlockFaces face;

    public HmcSchematicBlockFaced()
    {
        this(BlockFaces.ANY);
    }

    public HmcSchematicBlockFaced(BlockFaces face)
    {
        this.face = face;
    }

    @Override
    public BlockFaces getFace()
    {
        return this.face;
    }

    @Override
    public void setFace(BlockFaces face)
    {
        this.face = face;
    }

}
