package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.BlockFaces;

public interface SchematicBlockFaced<B extends SchematicBlock> extends SchematicBlock<B> {

    BlockFaces getFace();

    void setFace(BlockFaces face);

}
