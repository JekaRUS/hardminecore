package ru.jekarus.hardmine.api.schematic.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.block.utils.*;
import ru.jekarus.hardmine.impl.schematic.block.*;

public interface SchematicBlock<B extends SchematicBlock> {

    SchematicBlock ANY = new HmcSchematicBlockAny();

    static SchematicBlocks of(SchematicBlock... blocks)
    {
        return new HmcSchematicBlocks(blocks);
    }

    static SchematicBlockBase of(Material material)
    {
        if (material.isBlock())
        {
            return new HmcSchematicBlockBase(material);
        }
        else
        {
            throw new UnsupportedOperationException();
        }
    }

    static SchematicBlockBan ban(SchematicBlock block)
    {
        return new HmcSchematicBlockBan(block);
    }

    static SchematicBlockStairs stairs()
    {
        return new HmcSchematicBlockStairs();
    }

    static SchematicBlockStairs stairs(StairsMaterials material, BlockFaces face, BlockPositions position)
    {
        return new HmcSchematicBlockStairs(material, face, position);
    }

    static SchematicBlockSlab slab()
    {
        return new HmcSchematicBlockSlab();
    }

    static SchematicBlockSlab slab(SlabMaterials material, BlockPositions position)
    {
        return new HmcSchematicBlockSlab(material, position);
    }

    static SchematicBlockDoubleSlab doubleSlab()
    {
        return new HmcSchematicBlockDoubleSlab();
    }

    static SchematicBlockDoubleSlab doubleSlab(SlabMaterials material)
    {
        return new HmcSchematicBlockDoubleSlab(material);
    }

    static SchematicBlockFurnace furnace()
    {
        return new HmcSchematicBlockFurnace();
    }

    static SchematicBlockFurnace furnace(FurnaceMaterials material, BlockFaces face)
    {
        return new HmcSchematicBlockFurnace(material, face);
    }

    static SchematicBlockChest chest()
    {
        return new HmcSchematicBlockChest();
    }

    static SchematicBlockChest chest(ChestMaterials material, BlockFaces face)
    {
        return new HmcSchematicBlockChest(material, face);
    }

    static SchematicBlockWood wood()
    {
        return new HmcSchematicBlockWood();
    }

    static SchematicBlockWood wood(WoodMaterials material, WoodFaces face)
    {
        return new HmcSchematicBlockWood(material, face);
    }

    static SchematicBlockPlanks plank()
    {
        return new HmcSchematicBlockPlanks();
    }

    static SchematicBlockPlanks plank(PlanksMaterials material)
    {
        return new HmcSchematicBlockPlanks(material);
    }

    static SchematicBlockBed bed()
    {
        return new HmcSchematicBlockBed();
    }

    static SchematicBlockBed bed(BedParts part, BlockColors color, BlockFaces face)
    {
        return new HmcSchematicBlockBed(part, color, face);
    }

    static SchematicBlockWool wool()
    {
        return new HmcSchematicBlockWool();
    }

    static SchematicBlockWool wool(BlockColors color)
    {
        return new HmcSchematicBlockWool(color);
    }

    Material asBukkitMaterial();

    void place(Block block);

    boolean compare(Block block);

    B cloneBlock();

    B cloneBlock(BlockRotate rotate);

}