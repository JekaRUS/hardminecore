package ru.jekarus.hardmine.api.schematic;

import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;

public interface RotatableSchematicLayout extends SchematicLayout {

    boolean checkStructure(BlockRotate rotate, Block block);

    @Override
    default boolean checkStructure(Block block)
    {
        return this.checkStructure(BlockRotate.R_0, block);
    }

    boolean checkChangedBlock(BlockRotate rotate, Block minBlock, Block changedBlock);

    @Override
    default boolean checkChangedBlock(Block minBlock, Block changedBlock)
    {
        return this.checkChangedBlock(BlockRotate.R_0, minBlock, changedBlock);
    }

}
