package ru.jekarus.hardmine.api.structure;

import java.io.DataInputStream;
import java.io.IOException;

public interface StructureDeserializer {

    void read(DataInputStream stream) throws IOException;

}
