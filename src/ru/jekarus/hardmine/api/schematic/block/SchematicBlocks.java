package ru.jekarus.hardmine.api.schematic.block;

import java.util.Collection;

public interface SchematicBlocks extends SchematicBlock<SchematicBlocks> {

    Collection<SchematicBlock> getBlocks();

    void setBlocks(SchematicBlock... blocks);

    void addBlocks(Collection<SchematicBlock> blocks);

    void addBlocks(SchematicBlock... blocks);

    void removeBlocks(Collection<SchematicBlock> blocks);

    void removeBlocks(SchematicBlock... blocks);

    void setBlocks(Collection<SchematicBlock> blocks);

}
