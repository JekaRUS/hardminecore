package ru.jekarus.hardmine.impl.schematic;

import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.RotatableSchematic;
import ru.jekarus.hardmine.api.schematic.RotatableSchematicLayout;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.structure.type.StructureType;

public class HmcRotatableSchematic implements RotatableSchematic {

    private final StructureType type;
    private final RotatableSchematicLayout[] layout;

    public HmcRotatableSchematic(StructureType type, RotatableSchematicLayout[] layout)
    {
        this.type = type;
        this.layout = layout;
    }

    @Override
    public boolean checkStructure(Block block, BlockRotate rotate)
    {
        for (int y = 0; y < this.layout.length; y++)
        {
            if (!this.layout[y].checkStructure(rotate, block.getRelative(0, y, 0)))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean checkChangedBlock(Block minBlock, Block changedBlock, BlockRotate rotate)
    {
        return false; // FIXME: 03.01.2018
    }

}
