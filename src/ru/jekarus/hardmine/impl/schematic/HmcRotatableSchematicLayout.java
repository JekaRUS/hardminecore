package ru.jekarus.hardmine.impl.schematic;

import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.RotatableSchematicLayout;
import ru.jekarus.hardmine.api.schematic.SchematicLayout;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;
import ru.jekarus.hardmine.api.structure.type.StructureType;

import java.util.EnumMap;

public class HmcRotatableSchematicLayout implements RotatableSchematicLayout {

    private final EnumMap<BlockRotate, SchematicLayout> rotatedSchematics;
    private final StructureType type;

    public HmcRotatableSchematicLayout(SchematicBlock[][] layout)
    {
        this(null, layout);
    }

    public HmcRotatableSchematicLayout(StructureType type, SchematicBlock[][] layout)
    {
        this.type = type;
        this.rotatedSchematics = new EnumMap<>(BlockRotate.class);

        this.rotatedSchematics.put(BlockRotate.R_0, new HmcSchematicLayout(layout));
        this.rotatedSchematics.put(BlockRotate.R_360, new HmcSchematicLayout(layout));

        SchematicBlock[][] rotatedLayout = this.rotate(type, layout, BlockRotate.R_90);
        this.rotatedSchematics.put(BlockRotate.R_90, new HmcSchematicLayout(rotatedLayout));
        rotatedLayout = this.rotate(type, rotatedLayout, BlockRotate.R_180);
        this.rotatedSchematics.put(BlockRotate.R_180, new HmcSchematicLayout(rotatedLayout));
        rotatedLayout = this.rotate(type, rotatedLayout, BlockRotate.R_270);
        this.rotatedSchematics.put(BlockRotate.R_270, new HmcSchematicLayout(rotatedLayout));
    }

    @Override
    public boolean checkStructure(BlockRotate rotate, Block block)
    {
        return this.rotatedSchematics.get(rotate).checkStructure(block);
    }

    @Override
    public boolean checkChangedBlock(BlockRotate rotate, Block minBlock, Block changedBlock)
    {
        return false; // FIXME: 03.01.2018
    }

    private SchematicBlock[][] rotate(StructureType type, SchematicBlock[][] layout, BlockRotate rotate)
    {
        int sizeX = layout[0].length;
        int sizeZ = layout.length;

        SchematicBlock[][] rotatedLayout = new SchematicBlock[sizeX][sizeZ];

        int orgX = sizeX - 1;
        int orgZ = 0;

        for (int x = 0; x < sizeZ; x++)
        {
            for (int z = 0; z < sizeX; z++)
            {
                rotatedLayout[orgX][orgZ] = layout[x][z].cloneBlock(BlockRotate.R_90);
                orgX--;
            }
            orgX = sizeX - 1;
            orgZ++;
        }

//        StringBuilder in = new StringBuilder("\n");
//        StringBuilder out = new StringBuilder("\n");
//        for (int x = 0; x < sizeX; x++)
//        {
//            for (int z = 0; z < sizeZ; z++)
//            {
//                in.append(layout[x][z].asBukkitMaterial());
//                if (layout[x][z].asBukkitMaterial() == Material.COBBLESTONE_STAIRS) {
//                    in.append("(").append(((SchematicBlockStairs) layout[x][z]).getFace()).append(")");
//                }
//                in.append(" ");
//                out.append(rotatedLayout[x][z].asBukkitMaterial());
//                if (rotatedLayout[x][z].asBukkitMaterial() == Material.COBBLESTONE_STAIRS) {
//                    out.append("(").append(((SchematicBlockStairs) rotatedLayout[x][z]).getFace()).append(")");
//                }
//                out.append(" ");
//            }
//            in.append("\n");
//            out.append("\n");
//        }
//
//        System.out.println(in);
//        System.out.println(out);
//        System.out.println("---");

        return rotatedLayout;
    }

}