package ru.jekarus.hardmine.api.structure.access;

import com.sun.istack.internal.NotNull;
import ru.jekarus.hardmine.api.structure.StructureDeserializer;
import ru.jekarus.hardmine.api.structure.StructureSerializer;
import ru.jekarus.hardmine.api.structure.StructureUser;

public interface StructureAccessUser extends StructureSerializer, StructureDeserializer {

    @NotNull
    StructureUser getUser();

    boolean canOpen();

    boolean canOpenSettings();

    boolean canPlace();

    boolean canBreak();

    void setCanOpen(boolean value);

    void setCanOpenSettings(boolean value);

    void setCanPlace(boolean value);

    void setCanBreak(boolean value);

}
