package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Wool;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockWool;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockColors;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;

public class HmcSchematicBlockWool implements SchematicBlockWool {

    private BlockColors color;

    public HmcSchematicBlockWool()
    {
        this(BlockColors.ANY);
    }

    public HmcSchematicBlockWool(BlockColors color)
    {
        this.color = color;
    }

    @Override
    public BlockColors getColor()
    {
        return this.color;
    }

    @Override
    public void setColor(BlockColors color)
    {
        this.color = color;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return Material.WOOL;
    }

    @Override
    public void place(Block block)
    {
        block.setType(Material.WOOL);
        this.getWoolBlock(block).setColor(this.color.asBukkit());
    }

    @Override
    public boolean compare(Block block)
    {
        return this.asBukkitMaterial() == block.getType() && this.color.equalsColor(this.getWoolBlock(block).getColor());
    }

    private Wool getWoolBlock(Block block)
    {
        return ((Wool) block.getState().getData());
    }

    @Override
    public SchematicBlockWool cloneBlock()
    {
        return new HmcSchematicBlockWool(this.color);
    }

    @Override
    public SchematicBlockWool cloneBlock(BlockRotate rotate)
    {
        return this.cloneBlock();
    }

}
