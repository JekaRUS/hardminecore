package ru.jekarus.hardmine.api.schematic.block;

import ru.jekarus.hardmine.api.schematic.block.utils.SlabMaterials;

public interface SchematicBlockDoubleSlab extends SchematicBlock<SchematicBlockDoubleSlab> {

    SlabMaterials getMaterial();

    void setMaterial(SlabMaterials material);

}
