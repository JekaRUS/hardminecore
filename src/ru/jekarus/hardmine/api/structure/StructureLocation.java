package ru.jekarus.hardmine.api.structure;

import org.bukkit.World;
import org.bukkit.block.Block;
import ru.jekarus.hardmine.impl.structure.HmcStructureLocation;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public interface StructureLocation extends StructureSerializer, StructureDeserializer {

    World getWorld();

    int getX();

    int getY();

    int getZ();

    Collection<StructureChunk> getChunks();

    static StructureLocation of(Block block, int sizeX, int sizeZ)
    {
        if (block == null)
        {
            throw new NullPointerException();
        }
        World world = block.getWorld();
        if (world == null)
        {
            throw new NullPointerException();
        }

        Set<StructureChunk> chunks = new HashSet<>();
        chunks.add(StructureChunk.of(block.getChunk()));
        chunks.add(StructureChunk.of(block.getRelative(sizeX, 0, 0).getChunk()));
        chunks.add(StructureChunk.of(block.getRelative(0, 0, sizeZ).getChunk()));
        chunks.add(StructureChunk.of(block.getRelative(sizeX, 0, sizeZ).getChunk()));

        return new HmcStructureLocation(world, block.getX(), block.getY(), block.getZ(), chunks);
    }

}
