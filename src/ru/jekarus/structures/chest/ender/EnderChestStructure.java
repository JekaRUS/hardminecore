package ru.jekarus.structures.chest.ender;

import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.structure.HmcStructure;

public class EnderChestStructure extends HmcStructure {

    public EnderChestStructure(StructureType type, StructureUser owner, StructureAccess access, StructureData data)
    {
        super(type, owner, access, data);
    }

    public EnderChestStructure(StructureType type)
    {
        super(type);
    }

}
