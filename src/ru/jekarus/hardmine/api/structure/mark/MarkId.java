package ru.jekarus.hardmine.api.structure.mark;

public final class MarkId {

    private static int ID = 0;
    private final int id;

    private MarkId(int id)
    {
        this.id = id;
    }

    public static MarkId newMark()
    {
        return new MarkId(ID++);
    }

    public final int getOrdinal()
    {
        return this.id;
    }

    @Override
    public final boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        MarkId markId = (MarkId) o;

        return id == markId.id;
    }

    @Override
    public final int hashCode()
    {
        return id;
    }
}
