package ru.jekarus.structures.enchanting.table;

import ru.jekarus.hardmine.api.structure.StructureData;
import ru.jekarus.hardmine.api.structure.StructureUser;
import ru.jekarus.hardmine.api.structure.access.StructureAccess;
import ru.jekarus.hardmine.api.structure.type.StructureType;
import ru.jekarus.hardmine.impl.structure.HmcStructure;

public class EnchantingTableStructure extends HmcStructure {

    public EnchantingTableStructure(StructureType type, StructureUser owner, StructureAccess access, StructureData data)
    {
        super(type, owner, access, data);
    }

    public EnchantingTableStructure(StructureType type)
    {
        super(type);
    }

}
