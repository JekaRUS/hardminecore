package ru.jekarus.hardmine.impl.schematic;

import ru.jekarus.hardmine.api.schematic.RotatableSchematic;
import ru.jekarus.hardmine.api.schematic.RotatableSchematicBuilder;
import ru.jekarus.hardmine.api.schematic.RotatableSchematicLayout;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.structure.type.StructureType;

public class HmcRotatableSchematicBuilder extends HmcSchematicBuilder implements RotatableSchematicBuilder {

    public HmcRotatableSchematicBuilder(StructureType type)
    {
        super(type);
    }

    @Override
    public RotatableSchematic build()
    {
        RotatableSchematicLayout[] layouts = (RotatableSchematicLayout[]) this.compileShape();
        return new HmcRotatableSchematic(this.type, layouts);
    }

    @Override
    protected RotatableSchematicLayout[] createLayouts(int size)
    {
        return new RotatableSchematicLayout[size];
    }

    @Override
    protected RotatableSchematicLayout createLayout(SchematicBlock[][] layout)
    {
        return new HmcRotatableSchematicLayout(this.type, layout);
    }
}
