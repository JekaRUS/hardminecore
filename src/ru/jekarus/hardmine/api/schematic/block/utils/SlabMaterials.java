package ru.jekarus.hardmine.api.schematic.block.utils;

import org.bukkit.Material;

public enum SlabMaterials {

    ANY(Material.WOOD, Material.WOOD_STEP, Material.WOOD_DOUBLE_STEP)
            {
                @Override
                public boolean equalsType(Material type)
                {
                    return type == Material.WOOD_STEP || type == Material.STEP || type == Material.STONE_SLAB2 || type == Material.PURPUR_SLAB;
                }

                @Override
                public boolean equalsTypeDouble(Material type)
                {
                    return type == Material.WOOD_DOUBLE_STEP || type == Material.DOUBLE_STEP || type == Material.DOUBLE_STONE_SLAB2 || type == Material.PURPUR_DOUBLE_SLAB;
                }

                @Override
                public boolean equalsMaterial(Material material)
                {
                    return true;
                }
            },
    @Deprecated
    OAK(Material.WOOD, Material.WOOD_STEP, Material.WOOD_DOUBLE_STEP),
    COBBLESTONE(Material.COBBLESTONE, Material.STEP, Material.DOUBLE_STEP),
    BRICK(Material.BRICK, Material.STEP, Material.DOUBLE_STEP),
    STONE(Material.STONE, Material.STEP, Material.DOUBLE_STEP),
    SMOOTH(Material.SMOOTH_BRICK, Material.STEP, Material.DOUBLE_STEP),
    NETHER(Material.NETHER_BRICK, Material.STEP, Material.DOUBLE_STEP),
    SANDSTONE(Material.SANDSTONE, Material.STEP, Material.DOUBLE_STEP),
    QUARTZ(Material.QUARTZ_BLOCK, Material.STEP, Material.DOUBLE_STEP),
    @Deprecated
    SPRUCE(Material.WOOD, Material.WOOD_STEP, Material.WOOD_DOUBLE_STEP),
    @Deprecated
    BIRCH(Material.WOOD, Material.WOOD_STEP, Material.WOOD_DOUBLE_STEP),
    @Deprecated
    JUNGLE(Material.WOOD, Material.WOOD_STEP, Material.WOOD_DOUBLE_STEP),
    @Deprecated
    ACACIA(Material.WOOD, Material.WOOD_STEP, Material.WOOD_DOUBLE_STEP),
    @Deprecated
    DARK_OAK(Material.WOOD, Material.WOOD_STEP, Material.WOOD_DOUBLE_STEP),
    RED_SANDSTONE(Material.RED_SANDSTONE, Material.STONE_SLAB2, Material.DOUBLE_STONE_SLAB2),
    PURPUR(Material.PURPUR_BLOCK, Material.PURPUR_SLAB, Material.PURPUR_DOUBLE_SLAB);

    private final Material material;
    private final Material type;
    private final Material doubleType;

    SlabMaterials(Material material, Material type, Material doubleType)
    {
        this.material = material;
        this.type = type;
        this.doubleType = doubleType;
    }

    public Material asBukkit()
    {
        return this.type;
    }

    public Material asBukkitDouble()
    {
        return this.doubleType;
    }

    public Material getMaterial()
    {
        return this.material;
    }

    public boolean equalsType(Material type)
    {
        return this.type == type;
    }

    public boolean equalsTypeDouble(Material type)
    {
        return this.doubleType == type;
    }

    public boolean equalsMaterial(Material material)
    {
        return this.material == material;
    }

}
