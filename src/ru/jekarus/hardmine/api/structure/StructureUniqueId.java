package ru.jekarus.hardmine.api.structure;

import java.util.UUID;

public final class StructureUniqueId {

    private final Structure structure;
    private final UUID uniqueId;

    private StructureUniqueId(Structure structure, int x, int z)
    {
        this.structure = structure;

        long px = ((long) x) << 32;
        long pz = z & 0xffffffffL;

        this.uniqueId = new UUID(px | pz, structure.getData().getCreateTime());
    }

    public static StructureUniqueId newUniqueId(Structure structure)
    {
        if (structure == null)
        {
            throw new NullPointerException();
        }

        StructureData data = structure.getData();
        if (data == null)
        {
            throw new NullPointerException();
        }

        StructureLocation location = data.getLocation();
        if (location == null)
        {
            throw new NullPointerException();
        }

        return new StructureUniqueId(structure, location.getX(), location.getZ());
    }

    public final Structure getStructure()
    {
        return this.structure;
    }

    public final UUID getUniqueId()
    {
        return this.uniqueId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        StructureUniqueId that = (StructureUniqueId) o;

        return uniqueId.equals(that.uniqueId);
    }

    @Override
    public int hashCode()
    {
        return uniqueId.hashCode();
    }

}
