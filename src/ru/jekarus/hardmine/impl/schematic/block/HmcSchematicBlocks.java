package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlocks;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

public class HmcSchematicBlocks implements SchematicBlocks {

    protected Collection<SchematicBlock> blocks;

    public HmcSchematicBlocks()
    {
        this(new ArrayList<>());
    }

    public HmcSchematicBlocks(SchematicBlock... blocks)
    {
        this(Arrays.asList(blocks));
    }

    public HmcSchematicBlocks(Collection<SchematicBlock> blocks)
    {
        this.blocks = blocks;
    }

    @Override
    public Collection<SchematicBlock> getBlocks()
    {
        return this.blocks;
    }

    @Override
    public void setBlocks(SchematicBlock... blocks)
    {
        this.setBlocks(Arrays.asList(blocks));
    }

    @Override
    public void addBlocks(Collection<SchematicBlock> blocks)
    {
        this.blocks.addAll(blocks);
    }

    @Override
    public void addBlocks(SchematicBlock... blocks)
    {
        this.addBlocks(Arrays.asList(blocks));
    }

    @Override
    public void removeBlocks(Collection<SchematicBlock> blocks)
    {
        this.blocks.removeAll(blocks);
    }

    @Override
    public void removeBlocks(SchematicBlock... blocks)
    {
        this.removeBlocks(Arrays.asList(blocks));
    }

    @Override
    public void setBlocks(Collection<SchematicBlock> blocks)
    {
        this.blocks = blocks;
    }

    @Override
    public Material asBukkitMaterial()
    {
        Optional<SchematicBlock> first = this.blocks.stream().findFirst();
        if (first.isPresent())
        {
            return first.get().asBukkitMaterial();
        }
        else
        {
            return Material.AIR;
        }
    }

    @Override
    public void place(Block block)
    {
        Optional<SchematicBlock> first = this.blocks.stream().findFirst();
        if (first.isPresent())
        {
            first.get().place(block);
        }
        else
        {
            block.setType(Material.AIR);
        }
    }

    @Override
    public boolean compare(Block block)
    {
        for (SchematicBlock schematicBlock : this.blocks)
        {
            if (schematicBlock.compare(block))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public SchematicBlocks cloneBlock()
    {
        return new HmcSchematicBlocks(new ArrayList<>(this.blocks));
    }

    @Override
    public SchematicBlocks cloneBlock(BlockRotate rotate)
    {
        Collection<SchematicBlock> rotated = new ArrayList<>();
        this.blocks.forEach(block -> rotated.add(block.cloneBlock(rotate)));
        return new HmcSchematicBlocks(rotated);
    }
}
