package ru.jekarus.hardmine.impl.schematic.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlock;
import ru.jekarus.hardmine.api.schematic.block.SchematicBlockBan;
import ru.jekarus.hardmine.api.schematic.block.utils.BlockRotate;

public class HmcSchematicBlockBan implements SchematicBlockBan {

    protected SchematicBlock ban;

    public HmcSchematicBlockBan()
    {
        this(SchematicBlock.ANY);
    }

    public HmcSchematicBlockBan(SchematicBlock block)
    {
        this.ban = block;
    }

    @Override
    public Material asBukkitMaterial()
    {
        return Material.AIR;
    }

    @Override
    public void place(Block block)
    {
        block.setType(this.asBukkitMaterial());
    }

    @Override
    public boolean compare(Block block)
    {
        return !this.ban.compare(block);
    }

    @Override
    public SchematicBlockBan cloneBlock()
    {
        return new HmcSchematicBlockBan(this.ban.cloneBlock());
    }

    @Override
    public SchematicBlockBan cloneBlock(BlockRotate rotate)
    {
        return new HmcSchematicBlockBan(this.ban.cloneBlock(rotate));
    }

    @Override
    public SchematicBlock getBanned()
    {
        return this.ban;
    }

    @Override
    public void setBanned(SchematicBlock block)
    {
        this.ban = block;
    }
}
